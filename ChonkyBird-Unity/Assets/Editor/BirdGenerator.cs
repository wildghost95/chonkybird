﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BirdGenerator : EditorWindow
{
    public BirdInfo currentBird = new BirdInfo();
    public Bird bird;
    public float[] stageTimes;
    public Sprite[] maleSprites;
    public Sprite[] femaleSprites;


    [MenuItem("Custom Tool/Bird Generator")]
    public static void ShowWindow()
    {
        GetWindow(typeof(BirdGenerator));
    }

    private void OnGUI()
    {
        GUILayout.Label("Generate Chonky Bird");
       
        EditorGUILayout.HelpBox("Ensure a name is there to allow saving of object properly. In the current scene, game objects will be created. So please do it in an empty scene or clean it up later by deleting the created objects", MessageType.Info);
        currentBird.Name = EditorGUILayout.TextField("Name", currentBird.Name);
        currentBird.Type = (Bird_Type)EditorGUILayout.EnumPopup("Type", currentBird.Type);
        currentBird.Stage = (Bird_Stage)EditorGUILayout.EnumPopup("Stage ", currentBird.Stage);

        EditorGUILayout.HelpBox("Cooldown Time is in Hours, Hatch Time Reduction is in Seconds", MessageType.Info);
        currentBird.cooldownTime = EditorGUILayout.FloatField("Cooldown Time", currentBird.cooldownTime);
        currentBird.HatchTimeToReduce = EditorGUILayout.FloatField("Hatch Time Reduction", currentBird.HatchTimeToReduce);

        EditorGUILayout.HelpBox("Bird Stats are in Float values except for Size. These are the base stats of the bird", MessageType.Info);
        currentBird.Stats.Health = EditorGUILayout.FloatField("Health", currentBird.Stats.Health);
        currentBird.Stats.Chonk = EditorGUILayout.FloatField("Chonk", currentBird.Stats.Chonk);
        currentBird.Stats.Speed = EditorGUILayout.FloatField("Speed", currentBird.Stats.Speed);
        currentBird.Stats.Size = EditorGUILayout.IntField("Size", currentBird.Stats.Size);

        ScriptableObject target = this;
        SerializedObject so = new SerializedObject(target);
        SerializedProperty floatProperty = so.FindProperty("stageTimes");
        SerializedProperty mSpriteProperty = so.FindProperty("maleSprites");
        SerializedProperty fSpriteProperty = so.FindProperty("femaleSprites");

        EditorGUILayout.HelpBox("Stage Times refers to how long it takes to proceed between the stage in seconds. Last entry should always be 0 as Adults don't need to advance", MessageType.Info);
        EditorGUILayout.PropertyField(floatProperty, true); // True means show children
        EditorGUILayout.HelpBox("Male Sprite List : The sprites should be arranged in the order of Egg > Hatchling > Juvenile > Adult", MessageType.Info);
        EditorGUILayout.PropertyField(mSpriteProperty, true);
        EditorGUILayout.HelpBox("Female Sprite List : The sprites should be arranged in the order of Egg > Hatchling > Juvenile > Adult", MessageType.Info);
        EditorGUILayout.PropertyField(fSpriteProperty, true);
        so.ApplyModifiedProperties(); // Remember to apply modified properties

        if (GUILayout.Button("Generate Bird"))
        {
            GenerateBirdInfo(currentBird, stageTimes, maleSprites, femaleSprites);
        }
    }

    public static void GenerateBirdInfo(BirdInfo stat, float[] inStageTimes, Sprite[] inMaleSprites, Sprite[] inFemaleSprites)
    {
        GameObject bird = new GameObject();
        bird.name = stat.Name;
        stat.StageTimes = inStageTimes;
        stat.MaleStageSprites = inMaleSprites;
        stat.FemaleStageSprites = inFemaleSprites;
        bird.AddComponent<Bird>().CopyBirdInfo(stat);
        Debug.Log(PrefabUtility.SaveAsPrefabAsset(bird, $"{Application.dataPath}/Resources/Prefabs/Birds/Database/{bird.name}.prefab"));  
    }

}
