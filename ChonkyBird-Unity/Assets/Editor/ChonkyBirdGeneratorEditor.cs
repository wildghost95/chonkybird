﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class ChonkyBirdGeneratorEditor : EditorWindow
{
    public ChonkyBirdInfo currentBird = new ChonkyBirdInfo();

    public ChonkyBird chonkyBird;

    public float[] stageTimes;
    public Sprite[] sprites;

    [MenuItem("Custom Tool/ChonkyBirdGenerator")]
    public static void ShowWindow()
    {
        GetWindow(typeof(ChonkyBirdGeneratorEditor));
    }

    private void OnGUI()
    {
        GUILayout.Label("Generate Chonky Bird");

        EditorGUILayout.HelpBox("Ensure a name is there to allow saving of object properly. In the current scene, game objects will be created. So please do it in an empty scene or clean it up later", MessageType.Info);

        currentBird.NAME = EditorGUILayout.TextField("Name", currentBird.NAME);

        currentBird.FAMILY = (EnumConfig.BIRD_FAMILY)EditorGUILayout.EnumPopup("Family ", currentBird.FAMILY);
        currentBird.STAGE = (EnumConfig.BIRD_STAGE)EditorGUILayout.EnumPopup("Stage ", currentBird.STAGE);

        currentBird.STATS.HEALTH = EditorGUILayout.IntField("Health", currentBird.STATS.HEALTH);

        currentBird.STATS.CHONK = EditorGUILayout.IntField("Chonk", currentBird.STATS.CHONK);

        currentBird.STATS.SPEED = EditorGUILayout.IntField("Speed", currentBird.STATS.SPEED);

        currentBird.STATS.SIZE = EditorGUILayout.IntField("Size", currentBird.STATS.SIZE);

        ScriptableObject target = this;
        SerializedObject so = new SerializedObject(target);
        SerializedProperty floatProperty = so.FindProperty("stageTimes");
        SerializedProperty spriteProperty = so.FindProperty("sprites");

        EditorGUILayout.HelpBox("Stage Times refers to how long it takes to proceed between the stage in seconds. Last entry should always be 0 as Adults don't need to advance", MessageType.Info);
        EditorGUILayout.PropertyField(floatProperty, true); // True means show children
        EditorGUILayout.HelpBox("The sprites should be arranged in the order of Egg > Hatchling > Juvenile > Adult", MessageType.Info);
        EditorGUILayout.PropertyField(spriteProperty, true);
        so.ApplyModifiedProperties(); // Remember to apply modified properties


        //currentBird.SPRITE = (Sprite)EditorGUILayout.ObjectField("Bird ", currentBird.SPRITE, typeof(Sprite), false);



        if (GUILayout.Button("Generate Bird"))
        {
            GenerateBird(currentBird, stageTimes, sprites);
        }
    }

    /*
    public static void GenerateBird(ChonkyBirdInfo stat)
    {
        GameObject bird = new GameObject();
        bird.AddComponent<ChonkyBird>().Init(stat);
        Debug.Log(PrefabUtility.SaveAsPrefabAsset(bird, $"{Application.dataPath}/Prefab/Birds/{bird.name}.prefab"));
    }
    */

    public static void GenerateBird(ChonkyBirdInfo stat, float[] inStageTimes, Sprite[] inSprites)
    {
        GameObject bird = new GameObject();
        bird.AddComponent<ChonkyBird>().Init(stat, inStageTimes, inSprites);
        Debug.Log(PrefabUtility.SaveAsPrefabAsset(bird, $"{Application.dataPath}/Prefab/Birds/{bird.name}.prefab"));
    }
}
