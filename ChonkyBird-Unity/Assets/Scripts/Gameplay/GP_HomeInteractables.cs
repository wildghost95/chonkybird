﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GP_HomeInteractables : MonoBehaviour
{
    public Bird CopiedBird;

    private SpriteRenderer SpriteRenderer;
    private GP_HomeInteractableManager Manager;
    private GameObject BirdSpot;

    void Start()
    {
        SpriteRenderer = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        
    }

    public void AssignBirdInfo(BirdInfo birdToAssign, GameObject birdSpot, GP_HomeInteractableManager managerRef)
    {
        CopiedBird = new Bird(birdToAssign);
        //CopiedBird.CopyBirdInfo(birdToAssign);
        //CopiedBird = birdToAssign;

        BirdSpot = birdSpot;
        Manager = managerRef;

        UpdateSprite();
    }

    void UpdateSprite()
    {
        SpriteRenderer.sprite = CopiedBird.Info.GetSpriteList()[(int)CopiedBird.Info.Stage];
    }

    void OnMouseDown()
    {
        CopiedBird.Info.SendBackToNest();
        Debug.Log(this.name);

        if (TutorialManager.Instance.GetTutorialStatus() && TutorialManager.Instance.GetCurrentTutorialStage() == Tutorial_Stage.Stage_ReturnedJuvenile)
        {
            TutorialScreen.Instance.gameObject.SetActive(true);
        }

        //! @Des - I have a concern that this may end up deleting the Bird Info grabbed from the manager (not sure how likely, need testing).
        //! In the event that it does, just remove the reference before deleting and all should be fine.
        Destroy(this.gameObject);
    }
}
