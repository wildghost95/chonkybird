﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GP_BirdHunger : MonoBehaviour
{
    [SerializeField] private Slider hungerSlider;
    [SerializeField] private Bird birdRef = null;

    void Start()
    {
        // Grab reference of Bird Data from this GameObject for the Hunger values and cooldown timers
        birdRef = gameObject.GetComponent<Bird>();

        //! @Des - For now since there's nothing saving it (which should DEFINITELY BE IMPLEMENTED), just set the initial Hunger Timer here.
        if (birdRef.Info.Stage > Bird_Stage.EGG)
        {
            birdRef.Info.UpdateHungerTimer();
        }

        // Hunger slider is directly set in editor and saved to prefab so there shouldn't be any issue
    }


    // Update is called once per frame
    void Update()
    {
        // 1. Check if the Bird's already been fed. If it is, RemainingFeedCount will definitely be less than MaxFeedCount
        if (birdRef.Info.RemainingFeedCount < birdRef.Info.MaxFeedCount)
        {
            if (DateTime.Now > birdRef.Info.nextFeedCountReset)
            {
                birdRef.Info.ResetFeedCount();
            }
        }

        // 2. Calculate fullness (Implementation inside BirdInfo)
        birdRef.Info.DecreaseFullness();

        //! @Des - BirdInfo has function to return Fullness ratio (0 to 1) for the slider's display.
        hungerSlider.value = birdRef.Info.GetFullnessRatio();
    }
}
