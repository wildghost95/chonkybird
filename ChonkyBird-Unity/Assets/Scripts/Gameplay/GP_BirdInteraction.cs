﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GP_BirdInteraction : MonoBehaviour, IPointerClickHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    [Header("Interaction Settings")]
    public float minDragDistance;
    public int minTapCount = 3;
    public int minRubCount = 5;
    public int currentTapCount = 0;
    public int currentRubCount = 0;
    //public float durationDecreaseAmt = 1.0f;
    //public float maxDurationDecreaseAmt = 10.0f;

    [Header("Bird System Refs")]
    public GP_BirdTimer timerRef;
    //Add one for Hunger
    //Add one for fullness
    private Bird birdRef = null;

    [Header("Feed Object Reference")]
    private UI_FeedButton currentFocusedFeed = null;

    //private float currentDecreasedAmt = 0.0f;
    private bool bPressedDown = false;
    private Vector2 updatedInputScreenPos;
    private float screenPosDistance;
    private GameObject colliderRef;

    void Start()
    {
        timerRef = this.GetComponent<GP_BirdTimer>();
        birdRef = this.GetComponent<Bird>();

        switch (birdRef.Info.Stage)
        {
            case Bird_Stage.EGG:
                minTapCount = 3;
                minRubCount = 5;
                break;
            case Bird_Stage.HATCHLING:
                minTapCount = 3;
                minRubCount = 3;
                break;
            case Bird_Stage.JUVENILE:
                minTapCount = 3;
                minRubCount = 3;
                break;
            case Bird_Stage.ADULT:
                minTapCount = 3;
                minRubCount = 3;
                break;
            default:
                minTapCount = 3;
                minRubCount = 5;
                break;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //! @Des - Input hack on PC Editor to let us skip past the waiting.
        if (Input.GetKey(KeyCode.P))
        {
            timerRef.NextStageAchieved();
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        bPressedDown = true;
        updatedInputScreenPos = eventData.position;
        //Debug.Log("Click position : " + updatedInputScreenPos.ToString());

        if (birdRef.Info.Stage > Bird_Stage.EGG)
        {
            currentTapCount++;
            InteractedWithBird();
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        bPressedDown = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        screenPosDistance = (eventData.position - updatedInputScreenPos).magnitude;
        if (screenPosDistance >= minDragDistance)
        {
            //Debug.Log("Current magnitude : " + screenPosDistance.ToString());
            currentRubCount++;
            updatedInputScreenPos = eventData.position;
            InteractedWithBird();
            /*
            if (currentRubCount >= minRubCount)
            {
                currentRubCount = 0;
                timerRef.DecreaseDuration();
            }
            */
            /*
            if (currentDecreasedAmt < maxDurationDecreaseAmt)
            {
                currentDecreasedAmt += durationDecreaseAmt;
                timerRef.DecreaseDuration(durationDecreaseAmt);
            }
            */
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }

    public void SetCurrentFeed(UI_FeedButton newFeed)
    {
        currentFocusedFeed = newFeed;
    }

    //! May not need this function since if the feeds aren't being used then nothing should even happen
    //! Plus once a new feed is selected it'll override the current one.
    public void RemoveCurrentFeed()
    {
        currentFocusedFeed = null;
    }

    public void FeedBird(Feed_Type feedType)
    {
        Debug.Log("Function Called");
        //! @Des 2/4/2020 - Add in more functionality for this.
        //1. Remove 1 of the Bird Feed used
        InventoryManager.Instance.DecreaseFeed(feedType);

        //2. Call birdRef to feed the bird and handle hunger/happiness related issues inside Bird class.
        //   Hunger/Fullness meters should update themselves accordingly based on the changes to the bird
        birdRef.Info.FedBirdFeed(feedType);

        //3. Toggle tutorial, one time case
        if (TutorialManager.Instance.GetTutorialStatus() && TutorialManager.Instance.GetCurrentTutorialStage() == Tutorial_Stage.Stage_FedHatchling)
        {
            TutorialScreen.Instance.gameObject.SetActive(true);
        }
    }

    private void InteractedWithBird()
    {
        if (!TutorialScreen.Instance.gameObject.activeSelf)
        {
            // If the Bird isn't in Egg stage
            if (birdRef.Info.Stage > Bird_Stage.EGG)
            {
                // Increase bird happiness
                if (currentTapCount >= minTapCount || currentRubCount >= minRubCount)
                {
                    //! Increase happiness here
                    currentTapCount = 0;
                    currentRubCount = 0;
                    birdRef.Info.IncreaseHappiness(1.0f);
                }
            }
            else //If it is
            {
                // Decrease the remaining time instead
                timerRef.DecreaseDuration();

                if (TutorialManager.Instance.GetTutorialStatus() && TutorialManager.Instance.GetCurrentTutorialStage() == Tutorial_Stage.Stage_EggSpeedup)
                {
                    //Debug.Log("SDSD");
                    TutorialScreen.Instance.gameObject.SetActive(true);
                }
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        colliderRef = collider.gameObject;

        //! Check if the current feed is being used (cursor is being held down)
        if (currentFocusedFeed.GetCursorStatus())
        {
            //! When the Feed is dragged near the bird, set this bird as the focus so that the cursor knows which bird is being fed.
            currentFocusedFeed.SetFocusedBird(this);
            Debug.Log("Setting Bird Focus");
        }
        else
        {
            Debug.Log("Bird not in Focus");
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.gameObject == colliderRef)
        {
            currentFocusedFeed.RemoveFocusedBird();
        }

        colliderRef = null;
    }
}
