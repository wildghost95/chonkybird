﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GP_HomeInteractableManager : MonoBehaviour
{
    public List<GameObject> birdSpots = new List<GameObject>();
    public List<GameObject> takenSpots = new List<GameObject>();
    public GameObject wanderingBirdPrefab;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (!transform.GetChild(i).CompareTag("Interactables"))
            {
                birdSpots.Add(transform.GetChild(i).gameObject);
            }
        }

        foreach (BirdInfo bird in BirdManager.Instance.GetAllNestedBirds())
        {
            if (bird.isOutsideNest)
            {
                GameObject randomizedSpot = birdSpots[Random.Range(0, birdSpots.Count)];

                GameObject newBird = GameObject.Instantiate(wanderingBirdPrefab, randomizedSpot.transform);
                newBird.GetComponent<GP_HomeInteractables>().AssignBirdInfo(bird, randomizedSpot, this);

                takenSpots.Add(randomizedSpot);
                birdSpots.Remove(randomizedSpot);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BirdSentToNest(GameObject parentedSpot)
    {
        if (takenSpots.Contains(parentedSpot) && !birdSpots.Contains(parentedSpot))
        {
            takenSpots.Remove(parentedSpot);
            birdSpots.Add(parentedSpot);
        }
    }
}
