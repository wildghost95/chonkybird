﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GP_BirdTimer : MonoBehaviour
{
    private Bird birdRef = null;
    private float decreasedDuration = 0;
    private float initialTimeDifference = 0;

    [SerializeField] private GameObject sliderPrefab;
    private UI_TimeSlider uiSliderRef;

    void Start()
    {
        birdRef = gameObject.GetComponent<Bird>();

        if (TutorialManager.Instance.GetTutorialStatus())
        {
            //return;
        }

        //! @Des - Safety check to see if the Prefab is there. By default it should be, so this may be redundant.
        if (birdRef != null)
        {
            //birdRef.CalculateNextStageDate();
            //GameObject temp = GameObject.Instantiate(sliderPrefab, this.transform);
            uiSliderRef = gameObject.GetComponentInChildren<UI_TimeSlider>();
            birdRef.Info.UpdateCooldownStatus();
        }

        if (birdRef.Info.isIncubating)
        {
            Debug.Log("Previous Stage Time : " + birdRef.Info.previousStageDate.ToString());
            Debug.Log("Next Stage Time : " + birdRef.Info.nextStageDate.ToString());
            Debug.Log("Current Reduced Time : " + birdRef.Info.CurrentReducedHatchTime.ToString());

            if (DateTime.Now.AddSeconds(birdRef.Info.CurrentReducedHatchTime) > birdRef.Info.nextStageDate)
            {
                NextStageAchieved();
            }
            else
            {
                //Calculate the time difference between Next Stage and Current Stage
                TimeSpan ts = birdRef.Info.nextStageDate.Subtract(birdRef.Info.previousStageDate);
                initialTimeDifference = (float)ts.TotalSeconds;
            }
        }
    }

    void Update()
    {
        if (DateTime.Now.AddSeconds(birdRef.Info.CurrentReducedHatchTime) > birdRef.Info.nextStageDate)
        {
            NextStageAchieved();
        }

        birdRef.Info.CheckWanderingStatus();

        //float rat = ((float)birdRef.Info.previousStageDate.AddSeconds(birdRef.Info.CurrentReducedHatchTime).Subtract(birdRef.Info.nextStageDate).TotalSeconds) / initialTimeDifference;
        float rat = ((float)DateTime.Now.AddSeconds(birdRef.Info.CurrentReducedHatchTime).Subtract(birdRef.Info.nextStageDate).TotalSeconds) / initialTimeDifference; 
        uiSliderRef.ModifySlider(rat);
    }

    public void NextStageAchieved()
    {
        if (TutorialManager.Instance.GetTutorialStatus())
        {
            //return;
        }

        birdRef.IncreaseStage();
        AviaryManager.Instance.DisplayBirdStatus(birdRef.Info);

        switch (birdRef.Info.Stage)
        {
            case Bird_Stage.HATCHLING:
                birdRef.Info.UpdateHungerTimer();
                break;
            case Bird_Stage.JUVENILE:
                /*
                if (!TutorialManager.Instance.GetTutorialStatus())
                {
                    birdRef.Info.nextWanderingRollDate = DateTime.Now;
                }
                */
                break;
            case Bird_Stage.ADULT:
                //! The AddAdultBird func inside BirdManager should automatically remove the Bird from the NestedBirds List
                //! before adding it into AdultBirds list.
                BirdManager.Instance.AddAdultBird(birdRef.Info);
                Destroy(this.gameObject);
                break;
            default:
                break;
        }
    }

    public void DecreaseDuration()
    {
        //decreasedDuration += decreaseAmt;
        if (birdRef.Info.Stage == Bird_Stage.EGG)
        {
            if (!birdRef.Info.isInCooldown)
            {
                birdRef.Info.EnterCooldown();
                birdRef.Info.DecreaseHatchTime();
            }
            else
            {
                birdRef.Info.UpdateCooldownStatus();
            }
        }
        //Debug.Log(birdRef.Info.CurrentReducedHatchTime.ToString());
    }
}
