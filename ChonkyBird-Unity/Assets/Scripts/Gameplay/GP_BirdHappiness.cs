﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GP_BirdHappiness : MonoBehaviour
{
    [SerializeField] private Slider happinessSlider;
    [SerializeField] private Bird birdRef = null;

    void Start()
    {
        // Grab reference of Bird Data from this GameObject for the Hunger values and cooldown timers
        birdRef = gameObject.GetComponent<Bird>();

        // Happiness slider is directly set in editor and saved to prefab so there shouldn't be any issue
    }


    void Update()
    {
        // 1. Check if Bird Fullness is below 50%. Using ratio to check since it's between 0 to 1.
        if (birdRef.Info.GetFullnessRatio() < 0.5f)
        {
            birdRef.Info.DecreaseHappiness();
        }

        happinessSlider.value = birdRef.Info.GetHappinessRatio();
    }
}
