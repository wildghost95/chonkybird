﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumConfig
{
    //! Add on more Bird Types in this enum
    public enum BIRD_FAMILY 
    { 
        BIRD_BUDGIE = 0, 
        BIRD_COCKATIEL = 1
    };

    public enum BIRD_STAGE 
    { 
        EGG = 0, 
        HATCHING = 1, 
        JUVENILE = 2, 
        ADULT = 3
    };

}
