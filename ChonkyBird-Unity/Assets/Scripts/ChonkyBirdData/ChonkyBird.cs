﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChonkyBird : MonoBehaviour
{
    public ChonkyBirdInfo Info;

    public void Init(ChonkyBirdInfo inInfo)
    {
        Info = inInfo;
        gameObject.name = inInfo.NAME;
        Info.STAGETIME = inInfo.STAGETIME;
        Info.SPRITES = inInfo.SPRITES;
        gameObject.AddComponent<Image>().sprite = Info.SPRITES[(int)Info.STAGE];
        Info.bIsIncubating = false;
        //! TODO - More research on how to adjust time for this
        Info.nextStageDate = DateTime.UtcNow.AddSeconds(inInfo.STAGETIME[(int)Info.STAGE]);
        Info.PREFAB = this.gameObject;
    }

    public void Init(ChonkyBirdInfo inInfo, float[] inStageTimes, Sprite[] inSprites)
    {
        Info = inInfo;
        gameObject.name = inInfo.NAME;
        Info.STAGETIME = inStageTimes;
        Info.SPRITES = inSprites;
        gameObject.AddComponent<Image>().sprite = Info.SPRITES[(int)Info.STAGE];
        Info.bIsIncubating = false;
        //! TODO - More research on how to adjust time for this
        Info.nextStageDate = DateTime.UtcNow.AddSeconds(inStageTimes[(int)Info.STAGE]);
        Info.PREFAB = this.gameObject;
    }

    public void SetNextStageDate()
    {
        Info.nextStageDate = DateTime.UtcNow.AddSeconds(Info.STAGETIME[(int)Info.STAGE]);
    }

    public void SetIncubatingStatus(bool bStatus)
    {
        Info.bIsIncubating = bStatus;
    }
}

[System.Serializable]
public class ChonkyBirdInfo
{
    //! TODO - Modify Sprite into a Sprite array to be settled for each stage.
    //! This assumes Egg and Bird are merged into one, if so, need size of egg/bird. Adult size should be 0
    //! 
    public string NAME;
    [Space]
    public EnumConfig.BIRD_FAMILY FAMILY;
    public EnumConfig.BIRD_STAGE STAGE;
    public BIRD_STATS STATS;
    public bool bIsIncubating = false;
    public DateTime nextStageDate;
    [Space]
    public float[] STAGETIME = { 360.0f, 720.0f, 1440.0f, 0.0f }; //Time to advance between each stage.
    [Space]
    //public Sprite SPRITE = null; //Should convert into an Array of Sprites
    public Sprite[] SPRITES = null;
    public GameObject PREFAB = null;
}

[System.Serializable]
public struct BIRD_STATS
{
    public int HEALTH;
    public int CHONK;
    public int SPEED;
    public int SIZE;

    public BIRD_STATS(int inHealth, int inChonk, int inSpeed, int inSize)
    {
        HEALTH = inHealth;
        CHONK = inChonk;
        SPEED = inSpeed;
        SIZE = inSize;
    }
};

