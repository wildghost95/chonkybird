﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_HomeInteractables : MonoBehaviour
{
    public GameObject eggImg;
    private BirdInfo tempInfo = null;

    void Start()
    {
        if (BirdManager.Instance.GetNestedBirdCount() > 0)
        {
            eggImg.SetActive(true);
            while (tempInfo == null)
            {
                foreach (BirdInfo bird in BirdManager.Instance.GetAllNestedBirds())
                {
                    if (!bird.isOutsideNest)
                    {
                        tempInfo = bird;
                        //BirdInfo tempInfo = BirdManager.Instance.GetNestedBird(0);
                        //eggImg.GetComponent<Image>().sprite = tempInfo.Info.SPRITES[(int)tempInfo.Info.STAGE];
                        eggImg.GetComponent<Image>().sprite = tempInfo.GetSpriteList()[(int)tempInfo.Stage];
                        //break;
                    }
                }

                if (tempInfo == null)
                {
                    tempInfo = new BirdInfo();
                    eggImg.SetActive(false);
                }
            }
        }
        else
        {
            eggImg.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
