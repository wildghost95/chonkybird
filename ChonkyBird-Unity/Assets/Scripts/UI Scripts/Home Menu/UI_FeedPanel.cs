﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_FeedPanel : MonoBehaviour
{
    [Header("Dynamic UI Elements")]
    public Image feedImage;
    public Text feedText;
    public InputField inputAmount;

    [Header("UI Elements")]
    [SerializeField] private Sprite[] feedSprites;

    [Header("Shop Manager Reference")]
    [SerializeField] private UI_ShopMenuManager shopManager;


    private int purchaseAmt = 0;
    private Feed_Type selectedFeed;

    private void OnEnable()
    {
        purchaseAmt = 1;
        inputAmount.text = "1";
    }

    public void ModifyPurchaseItemDisplay(Feed_Type inFeedType)
    {
        selectedFeed = inFeedType;

        switch (inFeedType)
        {
            case Feed_Type.BASIC:
                feedText.text = "Basic";
                feedImage.sprite = feedSprites[0];
                break;
            case Feed_Type.PREMIUM:
                feedText.text = "Premium";
                feedImage.sprite = feedSprites[1];
                break;
            case Feed_Type.LUXURY:
                feedText.text = "Luxury";
                feedImage.sprite = feedSprites[2];
                break;
            case Feed_Type.CHAMPION:
                feedText.text = "Champion";
                feedImage.sprite = feedSprites[3];
                break;
            default:
                break;
        }
    }

    public void IncrementAmount()
    {
        purchaseAmt++;
        inputAmount.text = purchaseAmt.ToString();
    }

    public void DecrementAmount()
    {
        if (purchaseAmt > 1)
        {
            purchaseAmt--;
            inputAmount.text = purchaseAmt.ToString();
        }
    }

    public void InputFieldEdited()
    {
        if (int.TryParse(inputAmount.text, out purchaseAmt))
        {
            Debug.Log("Int validated");
        }
        else
        {
            inputAmount.text = "1";
            purchaseAmt = 1;
        }
    }

    public void ConfirmPurchase()
    {
        InventoryManager.Instance.AddFeed(selectedFeed, purchaseAmt);

        //! @Des : TODO - Add a check here or something. Inventory Manager should have a TryPurchase func
        //! That returns an error message if the funds isn't enough to purchase the amount they want.

        shopManager.ToggleFeedMenu(false);
    }
}
