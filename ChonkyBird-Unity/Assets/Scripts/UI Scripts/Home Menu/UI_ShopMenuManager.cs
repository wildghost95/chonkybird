﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_ShopMenuManager : MonoBehaviour
{
    [Header("Main 4 Menu Options")]
    public GameObject eggMenu;
    public GameObject foodMenu;
    public GameObject furnitureMenu;
    public GameObject chonkPointMenu;

    [Header("Sub Menu for Food Tab")]
    public GameObject foodPanel;
    public GameObject feedPanel;

    public GameObject mainStoreSection;

    private void OnDisable()
    {
        if (TutorialManager.Instance.GetTutorialStatus() && TutorialManager.Instance.GetCurrentTutorialStage() == Tutorial_Stage.Stage_BuyFood)
        {
            if (InventoryManager.Instance.GetFeedAmount(Feed_Type.BASIC) > 0)
            {
                TutorialScreen.Instance.gameObject.SetActive(true);
            }
        }
    }

    public void ToggleEggMenu(bool bOpen)
    {
        foodMenu.SetActive(!bOpen);
        furnitureMenu.SetActive(!bOpen);
        chonkPointMenu.SetActive(!bOpen);
        eggMenu.SetActive(bOpen);
        mainStoreSection.SetActive(!bOpen);
    }

    public void ToggleFoodMenu(bool bOpen)
    {
        eggMenu.SetActive(!bOpen);
        furnitureMenu.SetActive(!bOpen);
        chonkPointMenu.SetActive(!bOpen);
        foodMenu.SetActive(bOpen);
        mainStoreSection.SetActive(!bOpen);
    }

    public void ToggleFurnitureMenu(bool bOpen)
    {
        eggMenu.SetActive(!bOpen);
        foodMenu.SetActive(!bOpen);
        chonkPointMenu.SetActive(!bOpen);
        furnitureMenu.SetActive(bOpen);
        mainStoreSection.SetActive(!bOpen);
    }

    public void ToggleChonkPointMenu(bool bOpen)
    {
        eggMenu.SetActive(!bOpen);
        foodMenu.SetActive(!bOpen);
        furnitureMenu.SetActive(!bOpen);
        chonkPointMenu.SetActive(bOpen);
        mainStoreSection.SetActive(!bOpen);
    }

    public void ToggleFeedMenu(bool bOpen)
    {
        foodPanel.SetActive(!bOpen);
        feedPanel.SetActive(bOpen);
    }

    public void PurchaseBasicFeed()
    {
        ToggleFeedMenu(true);
        feedPanel.GetComponent<UI_FeedPanel>().ModifyPurchaseItemDisplay(Feed_Type.BASIC);
    }

    public void PurchasePremiumFeed()
    {
        ToggleFeedMenu(true);
        feedPanel.GetComponent<UI_FeedPanel>().ModifyPurchaseItemDisplay(Feed_Type.PREMIUM);
    }

    public void PurchaseLuxuryFeed()
    {
        ToggleFeedMenu(true);
        feedPanel.GetComponent<UI_FeedPanel>().ModifyPurchaseItemDisplay(Feed_Type.LUXURY);
    }

    public void PurchaseChampionFeed()
    {
        ToggleFeedMenu(true);
        feedPanel.GetComponent<UI_FeedPanel>().ModifyPurchaseItemDisplay(Feed_Type.CHAMPION);
    }
}
