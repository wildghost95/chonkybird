﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ResourcePanel : MonoBehaviour
{
    public Text cpText;
    public Text wpText;

    private void OnEnable()
    {
        cpText.text = ResourceManager.Instance.GetCP().ToString();
        wpText.text = ResourceManager.Instance.GetWP().ToString();
    }

    private void Start()
    {
        cpText.text = ResourceManager.Instance.GetCP().ToString();
        wpText.text = ResourceManager.Instance.GetWP().ToString();
    }
}
