﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_NestButton : MonoBehaviour
{
    bool isFocused = false;
    bool isForceDisabled = false;
    public Vector3 tutorialTriggerPos;
    public Tutorial_Stage[] disabledStages;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (TutorialManager.Instance.GetTutorialStatus() && TutorialManager.Instance.GetCurrentTutorialStage() == Tutorial_Stage.Stage_ClickNest)
        {
            if (gameObject.GetComponent<SpriteRenderer>().isVisible && Camera.main.WorldToScreenPoint(transform.position).x >= tutorialTriggerPos.x)
            {
                //Debug.Log("Nest On Screen");
                TutorialScreen.Instance.gameObject.SetActive(true);
            }
        }
    }

    private void OnMouseUp()
    {
        if (TutorialScreen.Instance.gameObject.activeSelf || isForceDisabled || CompareTutorialStage())
        {
            return;
        }

        if (isFocused)
        {
            SceneLoaderManager.Instance.LoadScene("Nest");
        }
    }

    public void ForceDisableNest(bool newStatus)
    {
        /*
        if (CompareTutorialStage())
        {
            isForceDisabled = true;
            return;
        }
        */

        isForceDisabled = newStatus;
    }

    bool CompareTutorialStage()
    {
        if (isForceDisabled)
        {
            return true;
        }

        foreach (Tutorial_Stage stage in disabledStages)
        {
            if (TutorialManager.Instance.GetCurrentTutorialStage() == stage)
            {
                return true;
            }
        }

        return false;
    }

    private void OnMouseEnter()
    {
        if (gameObject.GetComponent<SpriteRenderer>().isVisible && Camera.main.WorldToScreenPoint(transform.position).x >= tutorialTriggerPos.x)
        {
            isFocused = true;
        }
    }

    private void OnMouseExit()
    {
        isFocused = false;
    }
}
