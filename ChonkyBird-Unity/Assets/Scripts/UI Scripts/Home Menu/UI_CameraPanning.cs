﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_CameraPanning : MonoBehaviour
{
    Vector3 touchStart;
    [Header("Camera Zoom Settings")]
    public float minZoom = 300;
    public float maxZoom = 350;
    public float zoomSpeed = 5.0f;

    [Header("Camera Pan Settings")]
    public GameObject backgroundSprite;
    public Vector3 panningBoundaries;
    private bool isPanning = false;

    [Header("Menu Object References")]
    public GameObject[] menuPanels;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        #region Panning
        if (TutorialScreen.Instance.gameObject.activeSelf || CheckActiveMenuStatus())
        {
            return;
        }

        //! Seems to work for mobile as well. Need to check
        if (Input.GetMouseButtonDown(0))
        {
            touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Camera.main.transform.position += direction;
            ClampCameraPosition();

            //! This'll clamp nicely for horizontal pan but not vertical since Magnitude doesn't factor direction
            //Camera.main.transform.position = Vector3.ClampMagnitude(Camera.main.transform.position, 340.0f);
        }
        #endregion

        //! Check platform to determine which code for zooming. PC is for Editor testing
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            if (Input.GetKey(KeyCode.Equals))
            {
                Zoom(10.0f);
            }

            if (Input.GetKey(KeyCode.Minus))
            {
                Zoom(-10.0f);
            }
        }
        else if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            if (Input.touchCount == 2)
            {
                Touch tZero = Input.GetTouch(0);
                Touch tOne = Input.GetTouch(1);

                Vector2 tZeroPrePos = tZero.position - tZero.deltaPosition;
                Vector2 tOnePrePos = tOne.position - tOne.deltaPosition;

                float prevMag = (tZeroPrePos - tZeroPrePos).magnitude;
                float currentMag = (tZero.position - tOne.position).magnitude;

                float difference = currentMag - prevMag;

                Zoom(difference * 0.01f);
            }
        }

    }

    void Zoom(float increment)
    {
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize + (increment * Time.deltaTime * zoomSpeed), minZoom, maxZoom);
    }

    void ClampCameraPosition()
    {
        float boundaryX = (backgroundSprite.GetComponent<SpriteRenderer>().sprite.rect.width / 2) - (Camera.main.orthographicSize / 2) - 50.0f;
        //Debug.Log(boundaryX.ToString());
        float boundaryY = (backgroundSprite.GetComponent<SpriteRenderer>().sprite.rect.height / 2) - (Camera.main.orthographicSize);
        //Debug.Log(boundaryY.ToString());

        float clampX = Mathf.Clamp(Camera.main.transform.position.x, -boundaryX, boundaryX);
        float clampY = Mathf.Clamp(Camera.main.transform.position.y, -boundaryY, boundaryY);

        //float clampZ = Mathf.Clamp(Camera.main.transform.position.z, -panningBoundaries.z, panningBoundaries.z);
        float clampZ = Camera.main.transform.position.z;

        Camera.main.transform.position = new Vector3(clampX, clampY, clampZ);
    }

    bool CheckActiveMenuStatus()
    {
        foreach(GameObject go in menuPanels)
        {
            if (go.activeSelf == true)
            {
                return true;
            }
        }

        return false;
    }

    public bool GetPanningStatus()
    {
        return isPanning;
    }
}
