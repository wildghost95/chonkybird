﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ButtonToggle : MonoBehaviour
{
    [Header("Tutorial Stage to Activate Button")]
    public Tutorial_Stage StageToActivate;

    private Button buttonComp;

    // Start is called before the first frame update
    void Start()
    {
        buttonComp = GetComponent<Button>();

        if (TutorialManager.Instance.GetTutorialStatus())
        {
            if ((int)TutorialManager.Instance.GetCurrentTutorialStage() >= (int)StageToActivate)
            {
                buttonComp.interactable = true;
            }
            else
            {
                buttonComp.interactable = false;
            }
        }
        else
        {
            buttonComp.interactable = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
