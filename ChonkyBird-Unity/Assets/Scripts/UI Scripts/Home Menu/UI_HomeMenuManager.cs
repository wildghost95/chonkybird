﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_HomeMenuManager : MonoBehaviour
{
    [Header("Bottom Panel Buttons")]
    public GameObject inventoryMenu;
    public GameObject shopMenu;
    public GameObject compendiumMenu;
    public GameObject questMenu;
    public GameObject settingsMenu;

    [Header("Home Menu Interactables")]
    public UI_NestButton nestButton;

    [Header("Border Panels")]
    public GameObject topPanel;
    public GameObject bottomPanel;

    public void ToggleInventoryMenu(bool bOpen)
    {
        if (!TutorialScreen.Instance.gameObject.activeSelf)
        {
            inventoryMenu.SetActive(bOpen);
            /*
            shopMenu.SetActive(!bOpen);
            compendiumMenu.SetActive(!bOpen);
            questMenu.SetActive(!bOpen);
            settingsMenu.SetActive(!bOpen);
            */
            topPanel.SetActive(!bOpen);
            bottomPanel.SetActive(!bOpen);
            nestButton.ForceDisableNest(bOpen);
        }
    }

    public void ToggleShopMenu(bool bOpen)
    {
        if (!TutorialScreen.Instance.gameObject.activeSelf)
        {
            shopMenu.SetActive(bOpen);
            /*
            inventoryMenu.SetActive(!bOpen);
            compendiumMenu.SetActive(!bOpen);
            questMenu.SetActive(!bOpen);
            settingsMenu.SetActive(!bOpen);
            */
            topPanel.SetActive(!bOpen);
            bottomPanel.SetActive(!bOpen);
            nestButton.ForceDisableNest(bOpen);
        }
    }

    public void ToggleCompendium(bool bOpen)
    {
        if (!TutorialScreen.Instance.gameObject.activeSelf)
        {
            compendiumMenu.SetActive(bOpen);
            /*
            inventoryMenu.SetActive(!bOpen);
            shopMenu.SetActive(!bOpen);
            questMenu.SetActive(!bOpen);
            settingsMenu.SetActive(!bOpen);
            */
            topPanel.SetActive(!bOpen);
            bottomPanel.SetActive(!bOpen);
            nestButton.ForceDisableNest(bOpen);
        }
    }

    public void ToggleQuestMenu(bool bOpen)
    {
        if (!TutorialScreen.Instance.gameObject.activeSelf)
        {
            questMenu.SetActive(bOpen);
            /*
            inventoryMenu.SetActive(!bOpen);
            shopMenu.SetActive(!bOpen);
            compendiumMenu.SetActive(!bOpen);
            settingsMenu.SetActive(!bOpen);
            */
            topPanel.SetActive(!bOpen);
            bottomPanel.SetActive(!bOpen);
            nestButton.ForceDisableNest(bOpen);
        }
    }

    public void ToggleSettingsMenu(bool bOpen)
    {
        if (!TutorialScreen.Instance.gameObject.activeSelf)
        {
            settingsMenu.SetActive(bOpen);
            /*
            inventoryMenu.SetActive(!bOpen);
            shopMenu.SetActive(!bOpen);
            compendiumMenu.SetActive(!bOpen);
            questMenu.SetActive(!bOpen);
            */
            topPanel.SetActive(!bOpen);
            bottomPanel.SetActive(!bOpen);
            nestButton.ForceDisableNest(bOpen);
        }
    }

    public void LoadAviary()
    {
        //Debug.Log("Loading Aviary Now!");
        //SceneLoaderManager.Instance.LoadScene("Aviary");
    }
}
