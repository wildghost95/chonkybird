﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearSaveData : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //! Only to be called via delete button at start menu
    public void DeleteSaveData()
    {
        SaveManager.DeleteSaves();
    }
}
