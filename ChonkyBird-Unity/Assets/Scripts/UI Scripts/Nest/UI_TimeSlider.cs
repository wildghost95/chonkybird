﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_TimeSlider : MonoBehaviour
{
    private Slider displaySlider;

    public void Start()
    {
        displaySlider = gameObject.GetComponentInChildren<Slider>();
    }

    public void ModifySlider(float ratio)
    {
        //Debug.Log("Ratio : " + ratio.ToString());
        if (displaySlider != null)
        {
            displaySlider.value = Mathf.Abs(ratio);
        }
    }
}
