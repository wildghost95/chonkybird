﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_AviaryButtons : MonoBehaviour
{


    public void LoadScene(string sceneName)
    {
        SceneLoaderManager.Instance.LoadScene(sceneName);
    }

    public void Update()
    {
        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsEditor)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                LoadScene("HomeMenu");
            }
        }
    }

}
