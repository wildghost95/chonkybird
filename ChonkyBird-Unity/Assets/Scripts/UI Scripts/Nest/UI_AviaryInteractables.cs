﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_AviaryInteractables : MonoBehaviour
{
    public GameObject BirdNestAnchor;

    void Start()
    {
        UpdateNestStatus();
    }

    public void UpdateNestStatus()
    {
        ClearCurrentNest();
        if (BirdManager.Instance.GetNestedBirdCount() > 0)
        {
            if (BirdNestAnchor != null)
            {
                foreach (BirdInfo bird in BirdManager.Instance.GetAllNestedBirds())
                {
                    if (!bird.isOutsideNest)
                    {
                        GameObject tempBird = GameObject.Instantiate(BirdManager.Instance.GetBirdPrefab(), BirdNestAnchor.transform);
                        tempBird.GetComponent<Bird>().CopyBirdInfo(bird);
                    }
                }

                /*
                foreach (ChonkyBird bird in BirdManager.Instance.GetNestedBirds())
                {
                    //! Modify to spawn GameObject, assign info of bird over, and attach to Anchor
                    GameObject tempBird = GameObject.Instantiate(bird.Info.PREFAB, BirdNestAnchor.transform);
                    //bird.transform.SetParent(BirdNestAnchor.transform);
                    Debug.Log("B");
                }
                */
            }
            //eggImg.SetActive(true);
        }
        else
        {
            //Debug.Log("D");
        }
    }

    public void ClearCurrentNest()
    {
        for (int i = 0; i < BirdNestAnchor.transform.childCount; i++)
        {
            Destroy(BirdNestAnchor.transform.GetChild(i).gameObject);
        }
    }
}
