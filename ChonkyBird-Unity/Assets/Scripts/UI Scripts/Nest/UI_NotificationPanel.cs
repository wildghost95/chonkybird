﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_NotificationPanel : MonoBehaviour
{
    public GameObject notificationGrouping;
    public GameObject renamingGrouping;

    public Image birdImage;
    public Text birdStageText;
    public Text birdNameText;

    public InputField renameField;

    [Header("Renaming Panel")]
    public GameObject renamingPanel;
    private BirdInfo storedBirdInfo;

    void Start()
    {
        /*
        switch (PlayerPrefs.GetInt("FIRST_EGG"))
        {
            case 0:
                birdNameText.text = "Cockatiel";
                break;
            case 1:
                birdNameText.text = "Budgie";
                break;
            default:
                break;
        }
        */
    }

    private void OnEnable()
    {
        notificationGrouping.SetActive(true);
        renamingGrouping.SetActive(false);
    }

    private void OnDisable()
    {
        switch (TutorialManager.Instance.GetCurrentTutorialStage())
        {
            case Tutorial_Stage.Stage_ClickAviary:
                if (TutorialManager.Instance.GetTutorialStatus())
                {
                    TutorialScreen.Instance.gameObject.SetActive(true);
                }
                break;
            case Tutorial_Stage.Stage_Tutorial1Finished:
                if (TutorialManager.Instance.GetTutorialStatus())
                {
                    TutorialManager.Instance.IncrementTutorialIndex();
                    TutorialScreen.Instance.gameObject.SetActive(true);
                }
                break;
            case Tutorial_Stage.Stage_AdultBirdMessage:
                if (TutorialManager.Instance.GetTutorialStatus())
                {
                    //TutorialManager.Instance.IncrementTutorialIndex();
                    TutorialScreen.Instance.gameObject.SetActive(true);
                }
                break;
            default:
                break;
        }
    }

    public void UpdateBirdInfo(BirdInfo bird)
    {
        birdNameText.text = bird.Name;
        birdImage.sprite = bird.GetSpriteList()[(int)bird.Stage];
        birdStageText.text = bird.Stage.ToString();
        storedBirdInfo = bird;
    }

    public void RenamedBird()
    {
        string newBirdName = renameField.text;
        AviaryManager.Instance.UpdateBirdName(newBirdName);
    }

    public void ToggleRenamingPanel()
    {
        if (storedBirdInfo.Stage == Bird_Stage.HATCHLING)
        {
            renamingPanel.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
