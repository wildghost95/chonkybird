﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_LoadingScreenHandler : MonoBehaviour
{
    //@Des : Modify this to handle load via Asynchronous Loading so that it'll proceed only if everything necessary has been loaded.
    public Animator animationComponent;
    public GameObject beginnerEggSelection;
    public GameObject gameTitle;
    private bool bOpenBeginnerScreen = true;

    void OnEnable()
    {
        gameTitle.SetActive(false);
        StartCoroutine(SimulateFakeLoading());
    }

    public void SetBeginnerStatus(bool bIsFirstTime)
    {
        bOpenBeginnerScreen = bIsFirstTime;
    }

    IEnumerator SimulateFakeLoading()
    {
        yield return new WaitForSeconds(2.0f);

        animationComponent.enabled = false;

        if (bOpenBeginnerScreen)
        {
            beginnerEggSelection.SetActive(true);
            gameObject.SetActive(false);
        }
        else
        {
            SceneLoaderManager.Instance.LoadScene("HomeMenu");
        }

        yield return null;
    }
}
