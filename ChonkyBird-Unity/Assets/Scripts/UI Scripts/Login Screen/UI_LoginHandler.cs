﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_LoginHandler : MonoBehaviour
{
    //Simple handler. Checks PlayerPrefs for existence of PlayerID key. If existing, just load straight in. If not, 
    //Open up canvases to handle first time login/ ID creation
    public GameObject existingIDCanvas;
    public GameObject firstTimeCanvas;

    //Handler for the Loading Screen. Best to keep it all in 1 script
    public GameObject loadingScreenHandler;

    private void OnEnable()
    {
        // The actual check that should happen. Just checks if their ID exists.
        //if (PlayerPrefs.HasKey("PlayerID"))
        if (PlayerAccountManager.Instance.LoadPlayer())
        {
            //existingIDCanvas.SetActive(true);
            firstTimeCanvas.SetActive(false);
            LoadToGame(false);
        }
        else
        {
            firstTimeCanvas.SetActive(true);
            existingIDCanvas.SetActive(false);
        }
    }

    public void OnIDKeyedIn(InputField idField)
    {
        //Checks if they their name under character limit. Probably redundant since we capped the limit in the input field to 12
        if (idField.text.Length <= 12)
        {
            string playerID = idField.text;
            Debug.Log("Player ID is " + playerID.ToString());

            //Sets their PlayerID
            //PlayerPrefs.SetString("PlayerID", playerID);
            PlayerAccountManager.Instance.SetPlayerName(idField.text);

            LoadToGame(true);
        }
    }

    void LoadToGame(bool bIsFirstTime)
    {
        //Activates the loading screen. It should always be inactive by default.
        loadingScreenHandler.GetComponent<UI_LoadingScreenHandler>().SetBeginnerStatus(bIsFirstTime);
        loadingScreenHandler.SetActive(true);

        gameObject.SetActive(false);
    }
}
