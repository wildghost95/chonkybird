﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_EggSelection : MonoBehaviour
{
    public BirdInfo cockatielEgg;
    public BirdInfo budgieEgg;

    public void SelectedCockatielEgg()
    {
        Debug.Log("You've selected the Cockatiel!");
        //BirdManager.Instance.AddNestedBird(cockatielEgg);
        //BirdInfo egg = BirdDatabase.Instance.GetBirdFromDatabase("COCKATIEL");
        BirdManager.Instance.AddNestedBird("FIRST_EGG", BirdDatabase.Instance.GetBirdFromDatabase("COCKATIEL"));
        LoadMainMenu();
    }

    public void SelectedBudgieEgg()
    {
        Debug.Log("You've selected the Budgie!");
        //BirdManager.Instance.AddNestedBird(budgieEgg);
        BirdManager.Instance.AddNestedBird("FIRST_EGG", BirdDatabase.Instance.GetBirdFromDatabase("BUDGIE"));
        LoadMainMenu();
    }

    void LoadMainMenu()
    {
        //TutorialManager.Instance.ToggleTutorialStatus(true);
        SceneLoaderManager.Instance.LoadScene("HomeMenu");
    }
}
