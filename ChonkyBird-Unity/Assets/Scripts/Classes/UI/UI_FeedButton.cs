﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UI_FeedButton : MonoBehaviour, IPointerClickHandler, IDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler
{
    [Header("UI Element References")]
    public Image feedImage;
    public Text feedText;
    public Text feedAmtText;
    public Feed_Type feedType;

    [Header("Cursor Image Object Ref")]
    [SerializeField] private GameObject cursorSprite;

    //! Functional variables
    private bool IsCursorDown = false;
    private bool IsCursorOnBird = false;
    private GP_BirdInteraction currentBird = null;

    private void Start()
    {
        int feedAmount = InventoryManager.Instance.GetFeedAmount(feedType);
        //feedAmtText.text = feedAmount.ToString();

        feedText.text = feedType.ToString() + " : " + feedAmount.ToString();

        //! Failsafe to hide the cursor sprite if it was still visible for some reason.
        if (cursorSprite.activeSelf) cursorSprite.SetActive(false);
    }

    public void OnPointerClick(PointerEventData eventData)
    {

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        //! First check if the cursor is focused on a bird (moused over a bird etc.)
        if (!IsCursorOnBird)
        {
            //If not, do something or nothing.
            Debug.Log("Bird Not Fed");
        }
        else
        {
            //! Since a bird's been selected and we stop holding down the cursor
            //! Feed the bird
            currentBird.FeedBird(feedType);
            Debug.Log("Bird Fed");

            //! Hard update for the UI
            int feedAmount = InventoryManager.Instance.GetFeedAmount(feedType);
            //feedAmtText.text = feedAmount.ToString();

            feedText.text = feedType.ToString() + " : " + feedAmount.ToString();
        }

        //! Bool set to false to indicate cursor not held
        if (IsCursorDown) IsCursorDown = false;

        //! Hide the cursor sprite
        cursorSprite.SetActive(false);
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("Check");
        if (TutorialScreen.Instance.gameObject.activeSelf)
        {
            return;
        }

        //! Check if the Player even has enough of this Feed.
        if (InventoryManager.Instance.GetFeedAmount(feedType) > 0)
        {
            //! Bool set to true to indicate cursor held down
            if (!IsCursorDown) IsCursorDown = true;

            //! Show the cursor sprite and snap it to the cursor position
            cursorSprite.SetActive(true);
            cursorSprite.transform.position = eventData.position;

            //! Set the cursor's sprite
            cursorSprite.GetComponent<Image>().sprite = feedImage.sprite;

            //! Let all birds know which feed has been selected
            GP_BirdInteraction[] birdList = GameObject.FindObjectsOfType<GP_BirdInteraction>();

            foreach (GP_BirdInteraction bird in birdList)
            {
                bird.SetCurrentFeed(this);
            }
        }
        else
        {
            return;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        //! If the cursor is held down, make sure the sprite's 
        if (IsCursorDown)
        {
            cursorSprite.transform.position = eventData.position;
            //Vector3.MoveTowards(cursorSprite.transform.position, (Vector3)eventData.position, Time.deltaTime * 5.0f);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {

    }

    public void OnPointerExit(PointerEventData eventData)
    {

    }

    public void SetFocusedBird(GP_BirdInteraction newBird)
    {
        currentBird = newBird;
        IsCursorOnBird = true;
    }

    public void RemoveFocusedBird()
    {
        currentBird = null;
        IsCursorOnBird = false;
    }

    public bool GetCursorStatus()
    {
        return IsCursorDown;
    }
}
