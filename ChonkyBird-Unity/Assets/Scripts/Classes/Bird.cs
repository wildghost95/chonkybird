﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bird : MonoBehaviour
{
    public BirdInfo Info = new BirdInfo();
    [SerializeField] private Image imgComp;

    public Bird()
    {
        Info = new BirdInfo();
        //Stage Date shouldn't be set until incubating.
        //Stage Sprites are meant to be manually handled.
    }

    public Bird(BirdInfo inBird)
    {
        /*
        Info.Name = inBird.Name;
        Info.Type = inBird.Type;
        Info.Stage = inBird.Stage;
        Info.Stats = inBird.Stats;
        Info.Size = inBird.Size;
        Info.isIncubating = inBird.isIncubating;
        Info.StageTimes = inBird.StageTimes;
        Info.MaleStageSprites = inBird.MaleStageSprites;
        Info.FemaleStageSprites = inBird.FemaleStageSprites;
        */
        Info = inBird;
    }

    public Bird(string inName, Bird_Type inType, Bird_Stage inStage, Bird_Stats inStats, int inSize, bool inStatus, float[] inStageTimes, Sprite[] inMaleSprites, Sprite[] inFemaleSprites)
    {
        Info.Name = inName;
        Info.Type = inType;
        Info.Stage = inStage;
        Info.Stats = inStats;
        Info.Size = inSize;
        Info.isIncubating = inStatus;
        Info.StageTimes = inStageTimes;
        Info.MaleStageSprites = inMaleSprites;
        Info.FemaleStageSprites = inFemaleSprites;
    }

    public void CopyBirdInfo(BirdInfo infoToCopy)
    {
        Info = infoToCopy;
        UpdateSprite();
    }

    //! @Des : This is IMPORTANT for the BirdDatabase to use
    //! Else it'll return a direct reference to the Prefab's Stats, which means any changes in Gameplay also affect the Prefab.
    public BirdInfo GetCopiedInfo()
    {
        BirdInfo copiedInfo = new BirdInfo(Info);
        return copiedInfo;
    }

    public void IncreaseStage()
    {
        if (Info.Stage != Bird_Stage.ADULT)
        {
            Info.Stage += 1;
            Info.DetermineGender();
            UpdateSprite();
            Info.UpdateStageDate();

            if (Info.Stage == Bird_Stage.JUVENILE)
            {
                if (!TutorialManager.Instance.GetTutorialStatus())
                {
                    Info.UpdateWanderingStatus();
                }
            }
        }
        else
        {
            //! If Adult needs something to be done upon hitting the adult stage, like calculating their updated stats
        }

        BirdManager.Instance.SaveBirdsToFile();
    }

    public void ModifyStats(Bird_Stats Modifiers)
    {
        //Stat calculation here based off growth or smth.
    }

    public Sprite GetCurrentSprite()
    {
        if (Info.GetSpriteList().Length > 0)
        {
            return Info.GetSpriteList()[(int)Info.Stage];
        }
        else
        {
            return null;
        }
    }

    private void Start()
    {
        imgComp = gameObject.GetComponent<Image>();
        UpdateSprite();
    }

    private void UpdateSprite()
    {
        if (imgComp != null)
        {
            imgComp.sprite = GetCurrentSprite();
        }
    }

}

[System.Serializable]
public class BirdInfo
{
    //General Info (Applies to all Stages)
    public string Name;
    public Bird_Type Type;
    public Bird_Stage Stage;
    public Bird_Gender Gender;
    public Bird_Stats Stats;
    public Bird_Stats RaisedStats;
    public int Size;
    public bool isIncubating;
    
    public DateTime nextStageDate;
    public DateTime previousStageDate;
    public float[] StageTimes;
    public Sprite[] MaleStageSprites;
    public Sprite[] FemaleStageSprites;

    //Egg Stage only info
    public bool isInCooldown;
    public DateTime nextCooldownRelease;
    public float cooldownTime;
    public float HatchTimeToReduce;
    public float CurrentReducedHatchTime;

    //Hatchling & Juvenile info
    public float Fullness;
    public float Happiness;
    public bool isOutsideNest;

    public DateTime nextWanderingRollDate;
    //! @Des - Max Values defaulted to 100, only here just in case future changes include having variable fullness/happiness values for birds.
    public float MaxFullness = 100.0f;
    public float MaxHappiness = 100.0f;
    //! Fullness variables to cap how many times bird can be fed
    public bool isFeedable;
    public int MaxFeedCount;
    public int RemainingFeedCount;
    public DateTime nextFeedCountReset;
    public DateTime nextFullnessDecrease;
    //! Happiness variable
    public DateTime nextHappinessDecrease;

    public BirdInfo()
    {
        Name = "Bird";
        Type = Bird_Type.BUDGIE;
        Stage = Bird_Stage.EGG;
        Stats = new Bird_Stats();
        RaisedStats = new Bird_Stats();
        Size = 1;
        isIncubating = false;

        Fullness = 100.0f;
        Happiness = 0.0f;
        isOutsideNest = false;

        MaxFullness = 100.0f;
        MaxHappiness = 100.0f;

        StageTimes = new float[] { 360.0f, 720.0f, 1440.0f, 2880.0f };

        isInCooldown = false;
        cooldownTime = 1.0f;
        HatchTimeToReduce = 300.0f;
        CurrentReducedHatchTime = 0.0f;

        isFeedable = true;
        MaxFeedCount = 5;
        RemainingFeedCount = MaxFeedCount;
    }

    public BirdInfo(BirdInfo infoToCopy)
    {
        Name = infoToCopy.Name;
        Type = infoToCopy.Type;
        Stage = infoToCopy.Stage;
        Stats = infoToCopy.Stats;
        RaisedStats = infoToCopy.RaisedStats;
        Size = infoToCopy.Size;
        isIncubating = infoToCopy.isIncubating;

        Fullness = infoToCopy.Fullness;
        Happiness = infoToCopy.Happiness;
        isOutsideNest = infoToCopy.isOutsideNest;
        nextWanderingRollDate = infoToCopy.nextWanderingRollDate;

        MaxFullness = infoToCopy.MaxFullness;
        MaxHappiness = infoToCopy.MaxHappiness;

        StageTimes = infoToCopy.StageTimes;
        MaleStageSprites = infoToCopy.MaleStageSprites;
        FemaleStageSprites = infoToCopy.FemaleStageSprites;

        isInCooldown = infoToCopy.isInCooldown;
        nextCooldownRelease = infoToCopy.nextCooldownRelease;
        cooldownTime = infoToCopy.cooldownTime;
        HatchTimeToReduce = infoToCopy.HatchTimeToReduce;
        CurrentReducedHatchTime = infoToCopy.CurrentReducedHatchTime;

        isFeedable = infoToCopy.isFeedable;
        MaxFeedCount = infoToCopy.MaxFeedCount;
        RemainingFeedCount = MaxFeedCount;

        nextFeedCountReset = infoToCopy.nextFeedCountReset;
        nextFullnessDecrease = infoToCopy.nextFullnessDecrease;
        nextHappinessDecrease = infoToCopy.nextHappinessDecrease;
    }

    public Bird_SaveData GetSaveData()
    {
        Bird_SaveData savedData = new Bird_SaveData();

        savedData.BirdName = Name;
        savedData.BirdType = Type.ToString();
        savedData.BirdStage = Stage.ToString();
        savedData.BirdGender = Gender.ToString();

        savedData.BirdRaisedStats = new float[3];
        savedData.BirdRaisedStats[0] = RaisedStats.Health;
        savedData.BirdRaisedStats[1] = RaisedStats.Chonk;
        savedData.BirdRaisedStats[2] = RaisedStats.Speed;

        savedData.BirdIncubatingStatus = isIncubating;
        savedData.BirdNextStageDate = nextStageDate.ToBinary();
        savedData.BirdPreviousStageDate = previousStageDate.ToBinary();
        savedData.BirdCooldownStatus = isInCooldown;

        savedData.BirdNextCooldownReleaseDate = nextCooldownRelease.ToBinary();
        savedData.BirdCurrentReducedHatchTime = CurrentReducedHatchTime;
        savedData.BirdFullness = Fullness;
        savedData.BirdHappiness = Happiness;

        savedData.BirdNextWanderingRollDate = nextWanderingRollDate.ToBinary();
        savedData.BirdRemainingFeedCount = RemainingFeedCount;
        savedData.BirdNextFullnessDecrease = nextFullnessDecrease.ToBinary();
        savedData.BirdNextHappinessDecrease = nextHappinessDecrease.ToBinary();

        return savedData;
    }

    public void LoadSaveData(Bird_SaveData dataToLoad)
    {
        Name = dataToLoad.BirdName;
        Stage = (Bird_Stage) Enum.Parse(typeof(Bird_Stage), dataToLoad.BirdStage);
        Gender = (Bird_Gender)Enum.Parse(typeof(Bird_Gender), dataToLoad.BirdGender);

        RaisedStats.Health = dataToLoad.BirdRaisedStats[0];
        RaisedStats.Chonk = dataToLoad.BirdRaisedStats[1];
        RaisedStats.Speed = dataToLoad.BirdRaisedStats[2];

        isIncubating = dataToLoad.BirdIncubatingStatus;
        nextStageDate = DateTime.FromBinary(dataToLoad.BirdNextStageDate);
        previousStageDate = DateTime.FromBinary(dataToLoad.BirdPreviousStageDate);
        isInCooldown = dataToLoad.BirdCooldownStatus;

        nextCooldownRelease = DateTime.FromBinary(dataToLoad.BirdNextCooldownReleaseDate);
        CurrentReducedHatchTime = dataToLoad.BirdCurrentReducedHatchTime;
        Fullness = dataToLoad.BirdFullness;
        Happiness = dataToLoad.BirdHappiness;

        nextWanderingRollDate = DateTime.FromBinary(dataToLoad.BirdNextWanderingRollDate);
        RemainingFeedCount = dataToLoad.BirdRemainingFeedCount;
        nextFullnessDecrease = DateTime.FromBinary(dataToLoad.BirdNextFullnessDecrease);
        nextHappinessDecrease = DateTime.FromBinary(dataToLoad.BirdNextHappinessDecrease);
    }

    public void ChangeBirdName(string newName)
    {
        Name = newName;
    }

    public void UpdateStageDate()
    {
        CurrentReducedHatchTime = 0.0f;
        previousStageDate = DateTime.Now;
        nextStageDate = DateTime.Now.AddSeconds(StageTimes[(int)Stage]);
    }

    public void DetermineGender()
    {
        if (Stage == Bird_Stage.HATCHLING)
        {
            int randInt = UnityEngine.Random.Range(1, 11);

            if (randInt <= 6)
            {
                Gender = Bird_Gender.MALE;
            }
            else
            {
                Gender = Bird_Gender.FEMALE;
            }
        }
    }

    public void CheckWanderingStatus()
    {
        //! This should avoid updating the wandering status outright if the bird isn't in Juvenile stage or the Tutorial's Active.
        if (TutorialManager.Instance.GetTutorialStatus() || Stage != Bird_Stage.JUVENILE)
        {
            return;
        }

        if (DateTime.Now > nextWanderingRollDate)
        {
            UpdateWanderingStatus();
            nextWanderingRollDate = DateTime.Now.AddMinutes(15.0f);
        }
    }

    public void UpdateWanderingStatus()
    {
        int rand = UnityEngine.Random.Range(1, 101);
        if (rand <= 50)
        {
            isOutsideNest = true;
        }
        else
        {
            isOutsideNest = false;
        }
    }

    public void ForceBirdWanderingStatus(bool newStatus)
    {
        isOutsideNest = newStatus;
        Debug.Log(isOutsideNest.ToString());
    }

    public void SendBackToNest()
    {
        isOutsideNest = false;
        //! Add something like Happiness here.
        Debug.Log("Bird sent back to Nest");
    }

    public Sprite[] GetSpriteList()
    {
        if (Gender == Bird_Gender.MALE)
        {
            return MaleStageSprites;
        }
        else
        {
            return FemaleStageSprites;
        }
    }

    public void EnterCooldown()
    {
        isInCooldown = true;
        nextCooldownRelease = DateTime.Now.AddHours(cooldownTime);
        //Debug.Log(nextCooldownRelease.ToString());
        //Debug.Log(cooldownTime.ToString());
    }

    public void UpdateCooldownStatus()
    {
        if (isInCooldown)
        {
            if (DateTime.Now > nextCooldownRelease)
            {
                isInCooldown = false;
            }
        }
    }

    public void UpdateHungerTimer()
    {
        //! @Des : Currently set at 6 mins
        nextFullnessDecrease = DateTime.Now.AddMinutes(6.0f);
    }

    public void DecreaseHatchTime()
    {
        CurrentReducedHatchTime += HatchTimeToReduce;
    }

    public void SkipStage()
    {
        nextStageDate = DateTime.Now;
    }

    public void CalculateAdultStats()
    {
        Stats.Chonk += RaisedStats.Chonk;
        Stats.Speed += RaisedStats.Speed;
    }

    public void FedBirdFeed(Feed_Type inFeedType)
    {
        //! Check if the Bird's already been fed to the max.
        if (isFeedable)
        {
            int randInt;
            //! Check which type of Feed was given, and add the relevant stats and Fullness.
            switch (inFeedType)
            {
                case Feed_Type.BASIC:
                    randInt = UnityEngine.Random.Range(1, 11); //Randomize a number from 1 to 10
                    if (randInt >= 6)
                    {
                        RaisedStats.Chonk += 1;
                    }
                    else
                    {
                        RaisedStats.Speed += 1;
                    }
                    //! After feeding, check if the Happiness can be increased
                    IncreaseHappiness(5.0f);
                    break;
                case Feed_Type.PREMIUM:
                    RaisedStats.Chonk += 1;
                    RaisedStats.Speed += 2;
                    IncreaseHappiness(10.0f);
                    break;
                case Feed_Type.LUXURY:
                    RaisedStats.Chonk += 2;
                    RaisedStats.Speed += 1;
                    IncreaseHappiness(10.0f);
                    break;
                case Feed_Type.CHAMPION:
                    RaisedStats.Chonk += 3;
                    RaisedStats.Speed += 3;
                    IncreaseHappiness(10.0f);
                    break;
                default:
                    break;
            }

            //! Increase Fullness based on which Feed was given. Handled seperately for easier adjustments.
            IncreaseFullness(inFeedType);
            
            //! Reduce the remaining feed counts. @Des - Need to figure out a smarter way to clamp it back down.
            RemainingFeedCount--;
            if (RemainingFeedCount <= 0) isFeedable = false;

            //! Constantly refreshes each time you feed. Once you hit the max it'll be done anyway.
            SetFeedResetTime();

        }
        else
        {
            Debug.Log("Fed Bird Feed : Confirmed NOT Feedable");
            return;
        }
    }

    private void IncreaseFullness(Feed_Type inFeedType)
    {
        //! @Des : Values subject to change
        switch (inFeedType)
        {
            case Feed_Type.BASIC:
                Fullness += 10.0f;
                break;
            case Feed_Type.PREMIUM:
                Fullness += 20.0f;
                break;
            case Feed_Type.LUXURY:
                Fullness += 30.0f;
                break;
            case Feed_Type.CHAMPION:
                Fullness += 50.0f;
                break;
            default:
                break;
        }

        //! Locks the Fullness back down to 100 (Max).
        if (Fullness > MaxFullness) Fullness = MaxFullness;
    }

    public void DecreaseFullness()
    {
        if (Stage > Bird_Stage.EGG)
        {
            //! Implement a timer decrease function in BirdTimer and call this func
            if (DateTime.Now > nextFullnessDecrease)
            {
                Fullness -= MaxFullness / 5;
                UpdateHungerTimer();
            }
        }
    }

    public float GetFullnessRatio()
    {
        return (Fullness / MaxFullness);
    }

    public void SetFeedResetTime()
    {
        //! @Des - Need confirm with Kian/Marcus was it exactly Midnight to reset all Feed counts or by a fixed timer.
        //! Adds 5 minutes. So if at 1.00pm fed once = 1.05pm reset to 5. Fed 5 = 25 minutes, so 1.25pm.
        nextFeedCountReset = DateTime.Now.AddMinutes(5.0f);
    }

    public void ResetFeedCount()
    {
        RemainingFeedCount = MaxFeedCount;
        isFeedable = true;
    }

    public void IncreaseHappiness(float IncreaseValue)
    {
        //! @Des : Temporarily giving a flat 10% increase to Happiness (caps at 100%)
        //! Once final values and systems are finished this needs to be modified more
        //! e.g. Happiness increase differently based on feeds and playing
        Happiness += IncreaseValue;

        if (Happiness > MaxHappiness) Happiness = MaxHappiness;
    }

    public void DecreaseHappiness()
    {
        //! Implement a timer decrease function in BirdTimer and call this func
        if (DateTime.Now > nextHappinessDecrease)
        {
            nextHappinessDecrease = DateTime.Now.AddMinutes(15.0f);

            //! @Des - Throwing arbitrary value here since final values or formula not determined yet
            Happiness -= 5.0f;
            if (Happiness < 0.0f) Happiness = 0.0f;
        }
    }

    public float GetHappinessRatio()
    {
        return (Happiness / MaxHappiness);
    }
}

[System.Serializable]
public enum Bird_Type
{
    BUDGIE = 0,
    COCKATIEL = 1
};

[System.Serializable]
public enum Bird_Stage
{
    EGG = 0,
    HATCHLING = 1,
    JUVENILE = 2,
    ADULT = 3
};

[System.Serializable]
public enum Bird_Gender
{
    MALE = 0,
    FEMALE = 1
};

[System.Serializable]
public struct Bird_Stats
{
    public float Health;
    public float Chonk;
    public float Speed;
    public int Size;

    public Bird_Stats(float inHealth, float inChonk, float inSpeed, int inSize)
    {
        Health = inHealth;
        Chonk = inChonk;
        Speed = inSpeed;
        Size = inSize;
    }
};

[System.Serializable]
public struct Bird_SaveData
{
    //! Other than Name, the other 3 strings are enums but will be parsed via Enum.ParseString
    public string BirdName;
    public string BirdType;
    public string BirdStage;
    public string BirdGender;

    //! RaisedStats only needs 3 in the array for Health, Chonk, and Speed (health not being used but anticipating future uses)
    public float[] BirdRaisedStats;
    public bool BirdIncubatingStatus;
    public long BirdNextStageDate;
    public long BirdPreviousStageDate;
    public bool BirdCooldownStatus;
    public long BirdNextCooldownReleaseDate;
    public float BirdCurrentReducedHatchTime;

    public float BirdFullness;
    public float BirdHappiness;

    public long BirdNextWanderingRollDate;

    public int BirdRemainingFeedCount;
    public long BirdNextFeedCountReset;
    public long BirdNextFullnessDecrease;
    public long BirdNextHappinessDecrease;

    public Bird_SaveData(string Name, string Type, string Stage, string Gender, float[] Stats, bool IncubationStatus, long NextStageDate, long PrevStageDate, bool CooldownStatus,
                         long NextCooldownReleaseDate, float CurrentReducedHatchTime, float Fullness, float Happiness, long NextWanderingRollDate, int RemainingFeedCount, long NextFeedCountReset,
                         long NextFullnessDecrease, long NextHappinessDecrease)
    {
        BirdName = Name;
        BirdType = Type;
        BirdStage = Stage;
        BirdGender = Gender;
        BirdRaisedStats = Stats;
        BirdIncubatingStatus = IncubationStatus;
        BirdNextStageDate = NextStageDate;
        BirdPreviousStageDate = PrevStageDate;
        BirdCooldownStatus = CooldownStatus;
        BirdNextCooldownReleaseDate = NextCooldownReleaseDate;
        BirdCurrentReducedHatchTime = CurrentReducedHatchTime;
        BirdFullness = Fullness;
        BirdHappiness = Happiness;
        BirdNextWanderingRollDate = NextWanderingRollDate;
        BirdRemainingFeedCount = RemainingFeedCount;
        BirdNextFeedCountReset = NextFeedCountReset;
        BirdNextFullnessDecrease = NextFullnessDecrease;
        BirdNextHappinessDecrease = NextHappinessDecrease;
    }
};