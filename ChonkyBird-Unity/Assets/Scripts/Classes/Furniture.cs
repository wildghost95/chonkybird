﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Furniture : MonoBehaviour
{
    public string furnitureID;
    public Sprite furnitureSprite;
    public int wingPointCost;
    public int chonkPointCost;
    public int furnitureSize;
    public int currentFurnitureStack;
    public int maxFurnitureStack = 5;

    public Furniture()
    {
        furnitureID = "";
        furnitureSprite = null;
        wingPointCost = 0;
        chonkPointCost = 0;
        furnitureSize = 1;
        currentFurnitureStack = 0;
        maxFurnitureStack = 5;
    }

    public Furniture(string ID, Sprite sprite, int wpCost, int cpCost, int size, int maxStack)
    {
        furnitureID = ID;
        furnitureSprite = sprite;
        wingPointCost = wpCost;
        chonkPointCost = cpCost;
        furnitureSize = size;
        currentFurnitureStack = 0;
        maxFurnitureStack = maxStack;
    }

    public void CopyFurniture(Furniture other)
    {
        furnitureID = other.furnitureID;
        furnitureSprite = other.furnitureSprite;
        wingPointCost = other.wingPointCost;
        chonkPointCost = other.chonkPointCost;
        furnitureSize = other.furnitureSize;
        currentFurnitureStack = other.currentFurnitureStack;
        maxFurnitureStack = other.maxFurnitureStack;
    }

    //! Used by Inventory Manager to check if the amount in the stack has already hit the max
    public bool IsAtMaxStack()
    {
        return (currentFurnitureStack >= maxFurnitureStack);
    }

    public void IncreaseStack()
    {
        currentFurnitureStack++;
    }

    //! Used by Inventory Manager to compare the ID and check if there's already a furniture of this type in the inventory.
    public bool CompareID(string ID)
    {
        return string.Equals(furnitureID, ID);
    }
}
