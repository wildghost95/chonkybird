﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;

public class Feed : MonoBehaviour
{
    //! @Des : Currently unused. Might be unnecessary for the Feed class to exist and only the ENUM needed.
    public Feed_Type FeedType;
    public float FeedCost;
    public Sprite FeedImage;
}

public enum Feed_Type
{
    BASIC = 0,
    PREMIUM = 1,
    LUXURY = 2,
    CHAMPION = 3
};
