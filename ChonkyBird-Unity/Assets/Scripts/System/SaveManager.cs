﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveManager
{
    public static void DeleteSaves()
    {
        string playerDataPath = Application.persistentDataPath + "/player.dat";
        if (File.Exists(playerDataPath)) File.Delete(playerDataPath);

        string nestedBirdDataPath = Application.persistentDataPath + "/nestedbirds.dat";
        if (File.Exists(nestedBirdDataPath)) File.Delete(nestedBirdDataPath);

        string resourceDataPath = Application.persistentDataPath + "/resources.dat";
        if (File.Exists(resourceDataPath)) File.Delete(resourceDataPath);

        string feedDataPath = Application.persistentDataPath + "/feeds.dat";
        if (File.Exists(feedDataPath)) File.Delete(feedDataPath);

        string tutDataPath = Application.persistentDataPath + "/tutorial.dat";
        if (File.Exists(tutDataPath)) File.Delete(tutDataPath);
    }

    public static void SavePlayer (string playerName)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/player.dat";
        FileStream stream = new FileStream(path, FileMode.Create);

        string nameData = playerName;
        formatter.Serialize(stream, nameData);
        stream.Close();
    }

    public static string LoadPlayer()
    {
        string path = Application.persistentDataPath + "/player.dat";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            string nameData = formatter.Deserialize(stream) as string;
            stream.Close();

            return nameData;
        }
        else
        {
            return null;
        }
    }

    public static void SaveNestBirds(BirdInfo[] NestedBirds)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/nestedbirds.dat";
        FileStream stream = new FileStream(path, FileMode.Create);

        //! Need to figure out how to split apart which variables are to be saved and convert enums to strings.
        Bird_SaveData[] nestedBirds = new Bird_SaveData[NestedBirds.Length];

        for (int i = 0; i < NestedBirds.Length; i++)
        {
            BirdInfo tempInfo = NestedBirds[i];
            nestedBirds[i] = tempInfo.GetSaveData();
        }

        formatter.Serialize(stream, nestedBirds);
        stream.Close();
    }

    public static Bird_SaveData[] LoadNestBirds()
    {
        string path = Application.persistentDataPath + "/nestedbirds.dat";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Bird_SaveData[] nestedBirdData = formatter.Deserialize(stream) as Bird_SaveData[];
            stream.Close();

            //return null;
            return nestedBirdData;
        }
        else
        {
            return null;
        }
    }

    public static void SaveStoredBirds(BirdInfo[] StoredBirds)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/storedbirds.dat";
        FileStream stream = new FileStream(path, FileMode.Create);

        //! Need to figure out how to split apart which variables are to be saved and convert enums to strings.
        Bird_SaveData[] nestedBirds = new Bird_SaveData[StoredBirds.Length];

        for (int i = 0; i < StoredBirds.Length; i++)
        {
            BirdInfo tempInfo = StoredBirds[i];
            nestedBirds[i] = tempInfo.GetSaveData();
        }

        formatter.Serialize(stream, nestedBirds);
        stream.Close();
    }

    public static Bird_SaveData[] LoadStoredBirds()
    {
        string path = Application.persistentDataPath + "/storedbirds.dat";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Bird_SaveData[] storedBirdData = formatter.Deserialize(stream) as Bird_SaveData[];
            stream.Close();

            //return null;
            return storedBirdData;
        }
        else
        {
            return null;
        }
    }

    public static void SaveAdultBirds(BirdInfo[] AdultBirds)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/adultbirds.dat";
        FileStream stream = new FileStream(path, FileMode.Create);

        //! Need to figure out how to split apart which variables are to be saved and convert enums to strings.
        Bird_SaveData[] nestedBirds = new Bird_SaveData[AdultBirds.Length];

        for (int i = 0; i < AdultBirds.Length; i++)
        {
            BirdInfo tempInfo = AdultBirds[i];
            nestedBirds[i] = tempInfo.GetSaveData();
        }

        formatter.Serialize(stream, nestedBirds);
        stream.Close();
    }

    public static Bird_SaveData[] LoadAdultBirds()
    {
        string path = Application.persistentDataPath + "/adultbirds.dat";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Bird_SaveData[] adultBirdData = formatter.Deserialize(stream) as Bird_SaveData[];
            stream.Close();

            //return null;
            return adultBirdData;
        }
        else
        {
            return null;
        }
    }

    public static void SaveResources(int wingPointsToSave, int chonkPointsToSave)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/resources.dat";
        FileStream stream = new FileStream(path, FileMode.Create);

        int[] resourceData = new int[2];
        resourceData[0] = wingPointsToSave;
        resourceData[1] = chonkPointsToSave;

        formatter.Serialize(stream, resourceData);
        stream.Close();
    }

    public static int[] LoadResources()
    {
        string path = Application.persistentDataPath + "/resources.dat";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            int[] loadedResource = formatter.Deserialize(stream) as int[];
            stream.Close();

            return loadedResource;
        }
        else
        {
            return null;
        }
    }

    public static void SaveInventoryFeeds(int basic, int premium, int luxury, int champion)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/feeds.dat";
        FileStream stream = new FileStream(path, FileMode.Create);

        int[] feedData = new int[4];
        feedData[0] = basic;
        feedData[1] = premium;
        feedData[2] = luxury;
        feedData[3] = champion;

        formatter.Serialize(stream, feedData);
        stream.Close();
    }

    public static int[] LoadInventoryFeeds()
    {
        string path = Application.persistentDataPath + "/feeds.dat";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            int[] loadedFeeds = formatter.Deserialize(stream) as int[];
            stream.Close();

            return loadedFeeds;
        }
        else
        {
            return null;
        }
    }

    public static void SaveFurnitures(Furniture[] furnitures)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/furnitures.dat";
        FileStream stream = new FileStream(path, FileMode.Create);

        Furniture[] furnitureToSave = furnitures;

        formatter.Serialize(stream, furnitureToSave);
        stream.Close();
    }

    public static Furniture[] LoadFurnitures()
    {
        string path = Application.persistentDataPath + "/furnitures.dat";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Furniture[] loadedFurnitures = formatter.Deserialize(stream) as Furniture[];
            stream.Close();

            return loadedFurnitures;
        }
        else
        {
            return null;
        }
    }

    public static void SaveTutorialProgress(TutorialData data)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/tutorial.dat";
        FileStream stream = new FileStream(path, FileMode.Create);

        TutorialData saveData = data;

        formatter.Serialize(stream, saveData);
        stream.Close();
    }

    public static TutorialData LoadTutorialProgress()
    {
        string path = Application.persistentDataPath + "/tutorial.dat";

        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            TutorialData loadData = formatter.Deserialize(stream) as TutorialData;
            stream.Close();

            return loadData;
        }
        else
        {
            return null;
        }
    }
}
