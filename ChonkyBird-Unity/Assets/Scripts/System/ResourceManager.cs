﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager Instance { get; private set; }

    #region Private Variables
    [SerializeField] private int WingPoints;
    [SerializeField] private int ChonkPoints;
    #endregion

    private void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }

        LoadResourcesFromFile();
    }

    //! Function to handle resource loading from file
    public void LoadResourcesFromFile()
    {
        int[] savedResources = SaveManager.LoadResources();
        if (savedResources != null)
        {
            WingPoints = savedResources[0];
            ChonkPoints = savedResources[1];
        }
    }

    //! Function to handle resource saving to file
    public void SaveResourcesToFile()
    {
        SaveManager.SaveResources(WingPoints, ChonkPoints);
    }

    public void IncreaseWP(int increaseAmt)
    {
        WingPoints += increaseAmt;

        //! May need to introduce a cap here on Wing Points
        SaveResourcesToFile();
    }

    public void DecreaseWP(int decreaseAmt)
    {
        WingPoints -= decreaseAmt;

        SaveResourcesToFile();
    }

    public int GetWP()
    {
        return WingPoints;
    }

    public void IncreaseCP(int increaseAmt)
    {
        ChonkPoints += increaseAmt;
        SaveResourcesToFile();
    }

    public void DecreaseCP(int decreaseAmt)
    {
        ChonkPoints -= decreaseAmt;
        SaveResourcesToFile();
    }

    public int GetCP()
    {
        return ChonkPoints;
    }
}
