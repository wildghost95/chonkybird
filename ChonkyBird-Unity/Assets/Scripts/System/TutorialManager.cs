﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public static TutorialManager Instance { get; private set; }

    [SerializeField] private bool IsTutorialActive = true;
    [SerializeField] private int CurrentTutorialIndex = 0;
    [SerializeField] private Tutorial_Stage CurrentTutorialStage = Tutorial_Stage.Stage_Camera;

    private void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            Instance = this;
        }
        else 
        {
            Destroy(this.gameObject);
        }

        LoadTutorialProgress();
    }

    private void Start()
    {
    }

    public void SaveTutorialProgress()
    {
        TutorialData dataToSave = new TutorialData(IsTutorialActive, CurrentTutorialIndex, CurrentTutorialStage.ToString());
        SaveManager.SaveTutorialProgress(dataToSave);
    }

    void LoadTutorialProgress()
    {
        TutorialData loadedData = SaveManager.LoadTutorialProgress();

        IsTutorialActive = loadedData.TutorialStatus;
        CurrentTutorialIndex = loadedData.TutorialIndex;
        CurrentTutorialStage = (Tutorial_Stage)Enum.Parse(typeof(Tutorial_Stage), loadedData.TutorialStage);
    }

    public void ToggleTutorialStatus(bool Status)
    {
        IsTutorialActive = Status;
    }

    public bool GetTutorialStatus()
    {
        /*
        if (CurrentTutorialStage == Tutorial_Stage.Stage_Tutorial1Finished || CurrentTutorialStage == Tutorial_Stage.Stage_Tutorial2Finished)
        {
            return true;
        }
        else
        {
        }
        */
        return IsTutorialActive;
    }

    public void IncrementTutorialIndex()
    {
        CurrentTutorialIndex++;
        CurrentTutorialStage++;

        Debug.Log(CurrentTutorialStage);
        //SaveTutorialProgress();
    }

    public Tutorial_Stage GetCurrentTutorialStage()
    {
        return CurrentTutorialStage;
    }

    public void OpenedTutorialScreen()
    {
        SaveTutorialProgress();
    }

    public void ClosedTutorialScreen()
    {
        if (IsTutorialActive)
        {
            IncrementTutorialIndex();
        }

        switch (CurrentTutorialStage)
        {
            case Tutorial_Stage.Stage_Camera:
                break;
            case Tutorial_Stage.Stage_ClickNest:
                break;
            case Tutorial_Stage.Stage_Egg:
                break;
            case Tutorial_Stage.Stage_EggInfo:
                break;
            case Tutorial_Stage.Stage_EggSpeedup:
                break;
            case Tutorial_Stage.Stage_ClickAviary:
                //TutorialScreen.Instance.gameObject.SetActive(true);
                BirdManager.Instance.GetNestedBird(0).SkipStage();
                break;
            case Tutorial_Stage.Stage_Store:
                break;
            case Tutorial_Stage.Stage_BuyFood:
                break;
            case Tutorial_Stage.Stage_FeedBird:
                break;
            case Tutorial_Stage.Stage_HappinessMeter:
                break;
            case Tutorial_Stage.Stage_Hatchling:
                break;
            case Tutorial_Stage.Stage_Tutorial1Finished:
                //Force to Juvenile here
                BirdManager.Instance.GetNestedBird(0).SkipStage();
                break;
            case Tutorial_Stage.Stage_AdultBirdMessage:
                BirdManager.Instance.GetNestedBird(0).SkipStage();
                break;
            case Tutorial_Stage.Stage_Tutorial2Finished:
                ToggleTutorialStatus(false);
                break;
            default:
                break;
        }
    }
}

public enum Tutorial_Stage
{
    Stage_TutorialDisabled = 0,
    Stage_Camera = 1,
    Stage_ClickNest = 2,
    Stage_Egg = 3,
    Stage_EggInfo = 4,
    Stage_EggSpeedup = 5,
    Stage_ClickAviary = 6,
    Stage_Store = 7,
    Stage_BuyFood = 8,
    Stage_FeedBird = 9,
    Stage_FedHatchling = 10,
    Stage_HappinessMeter = 11,
    Stage_Hatchling = 12,
    Stage_Tutorial1Finished = 13,
    Stage_Juvenile = 14,
    Stage_FindJuvenile = 15,
    Stage_ReturnedJuvenile = 16,
    Stage_BackToNest = 17,
    Stage_Adult = 18,
    Stage_AdultBirdMessage = 19,
    Stage_Tutorial2Finished = 20
}

[System.Serializable]
public class TutorialData
{
    public bool TutorialStatus;
    public int TutorialIndex;
    public string TutorialStage;

    public TutorialData(bool Status, int Index, string Stage)
    {
        TutorialStatus = Status;
        TutorialIndex = Index;
        TutorialStage = Stage;
    }
};