﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdDatabase : MonoBehaviour
{
    public static BirdDatabase Instance { get; private set; }

    [SerializeField] private string BirdDatabaseFilePath = "Prefabs/Birds/Database";

    Dictionary<string, BirdInfo> BirdDictionary = new Dictionary<string, BirdInfo>();

    public Object[] objs; //! Purely for visual debugging. Actual code uses BirdDictionary ^

    void Awake()
    {
        //! Assign Instance
        if (Instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }

        //! Loads all Objects from the Path. The birds generated via the new Generator should be in the Prefabs/Birds/Database folder.
        objs = Resources.LoadAll(BirdDatabaseFilePath);

        //! Make a GameObject List for casting.
        List<GameObject> gameObjList = new List<GameObject>();

        //! Cast the Object retrieved to GameObject so we can access the components
        foreach (Object obj in objs)
        {
            gameObjList.Add((GameObject)obj);
        }

        //! Get relevant info needed. The birdKey is the unique key to retrieve the info from the Dictionary.
        //! The info is the stats and such of the bird.
        foreach (GameObject gameObj in gameObjList)
        {
            string birdKey = gameObj.GetComponent<Bird>().Info.Type.ToString();
            //Debug.Log(birdKey);
            BirdInfo info = gameObj.GetComponent<Bird>().GetCopiedInfo();
            BirdDictionary.Add(birdKey, info);
        }
    }

    public BirdInfo GetBirdFromDatabase(string birdKey)
    {
        //! Checks the Dictionary for the Bird by checking the birdKey. If it exists, it'll return a copy.
        BirdInfo info;

        if (BirdDictionary.TryGetValue(birdKey, out info))
        {
            //! Further modifications can be added here in the future if needed such as Stat Variation.
            //Debug.Log("Request Successful");
            return info;
        }
        else
        {
            return null;
        }
    }
}
