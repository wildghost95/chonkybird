﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TutorialScreen : MonoBehaviour
{
    public static TutorialScreen Instance { get; private set; }

    [Header("Tutorial Part 1 Vars (Camera)")]
    public TutorialDisplayText TextT1;

    [Header("Tutorial Part 2 Vars (Entering Nest)")]
    public TutorialDisplayText TextT2;
    public UI_NestButton NestButton;

    [Header("Tutorial Part 3 Vars (Hatching Egg)")]
    public TutorialDisplayText TextT3;

    [Header("Tutorial Part 4 Vars (Rubbed Egg)")]
    public TutorialDisplayText TextT4;

    [Header("Tutorial Part 5 Vars (Speedup Egg)")]
    public TutorialDisplayText TextT5;

    [Header("Tutorial Part 6 Vars (Clicking Aviary)")]
    public TutorialDisplayText TextT6;
    public Button aviaryButton;

    [Header("Tutorial Part 7 Vars (Opening Shop)")]
    public TutorialDisplayText TextT7;

    [Header("Tutorial Part 8 Vars (Return to Nest)")]
    public TutorialDisplayText TextT8;

    [Header("Tutorial Part 9 Vars (Feeding Bird)")]
    public TutorialDisplayText TextT9;

    [Header("Tutorial Part 10 Vars (Bird Happiness & Hunger)")]
    public TutorialDisplayText TextT10;

    [Header("Tutorial Part 11 Vars (Happiness Meter Explanation)")]
    public TutorialDisplayText TextT11;

    [Header("Tutorial Part 12 Vars (Speed up to Juvenile)")]
    public TutorialDisplayText TextT12;

    //! @Des : T12 and T13 Texts left out in case we need to add them in later, mainly for speeding up Hatchling > Juvenile

    [Header("Tutorial Part 14 Vars (Juvenile)")]
    public TutorialDisplayText TextT14;

    [Header("Tutorial Part 15 Vars (Finding Juvenile)")]
    public TutorialDisplayText TextT15;

    [Header("Tutorial Part 16 Vars (Returned Juvenile to Nest)")]
    public TutorialDisplayText TextT16;

    [Header("Tutorial Part 17 Vars (Back to Nest)")]
    public TutorialDisplayText TextT17;

    [Header("Tutorial Part 18 Vars (Speed up to Adult)")]
    public TutorialDisplayText TextT18;

    [Header("Tutorial Part 19 Vars (Adult Bird Details)")]
    public TutorialDisplayText TextT19;

    [Header("Tutorial Screen UI Elements")]
    public Button ContinueButton;
    public TextMeshProUGUI BodyText; 

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

        //gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        /*
        if (Camera.main.GetComponent<UI_CameraPanning>() != null)
        {
            Camera.main.GetComponent<UI_CameraPanning>().enabled = false;
        }
        */
        //Time.timeScale = 0;
        UpdateTutorialUI();
        TutorialManager.Instance.OpenedTutorialScreen();
    }

    private void OnDisable()
    {
        /*
        if (Camera.main.GetComponent<UI_CameraPanning>() != null)
        {
            Camera.main.GetComponent<UI_CameraPanning>().enabled = true;
        }
        */
        //Time.timeScale = 1;

        TutorialManager.Instance.ClosedTutorialScreen();
    }

    private void Start()
    {
        if (TutorialManager.Instance.GetTutorialStatus())
        {
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }

        UpdateTutorialUI();
    }

    public void CloseScreen() //Used by the Continue button in the tutorial screen in case there's any custom callbacks we want to do.
    {
        switch (TutorialManager.Instance.GetCurrentTutorialStage())
        {
            case Tutorial_Stage.Stage_Camera:
                break;
            case Tutorial_Stage.Stage_ClickNest:
                break;
            case Tutorial_Stage.Stage_Egg:
                TutorialManager.Instance.IncrementTutorialIndex();
                UpdateTutorialUI();
                return;
            case Tutorial_Stage.Stage_EggInfo:
                break;
            case Tutorial_Stage.Stage_EggSpeedup:
                break;
            case Tutorial_Stage.Stage_ClickAviary:
                aviaryButton.interactable = true;
                break;
            case Tutorial_Stage.Stage_Store:
                NestButton.ForceDisableNest(true);
                break;
            case Tutorial_Stage.Stage_BuyFood:
                NestButton.ForceDisableNest(false);
                break;
            case Tutorial_Stage.Stage_FeedBird:
                //aviaryButton.interactable = false;
                break;
            case Tutorial_Stage.Stage_FedHatchling:
                TutorialManager.Instance.IncrementTutorialIndex();
                UpdateTutorialUI();
                return;
            case Tutorial_Stage.Stage_HappinessMeter:
                TutorialManager.Instance.IncrementTutorialIndex();
                UpdateTutorialUI();
                return;
            case Tutorial_Stage.Stage_Hatchling:
                aviaryButton.interactable = true;
                break;
            case Tutorial_Stage.Stage_Juvenile:
                break;
            case Tutorial_Stage.Stage_FindJuvenile:
                NestButton.ForceDisableNest(true);
                break;
            case Tutorial_Stage.Stage_ReturnedJuvenile:
                NestButton.ForceDisableNest(false);
                break;
            case Tutorial_Stage.Stage_BackToNest:
                TutorialManager.Instance.IncrementTutorialIndex();
                UpdateTutorialUI();
                return;
            default:
                break;
        }

        gameObject.SetActive(false);
    }

    public void UpdateTutorialUI() //! Used to enable/disable relevant buttons to each step of the tutorial & update the UI to display the correct text.
    {
        //! @Des : Better optimization is to use a function that sorts through each of the TutorialElements set in a List/Array
        //! Better update it at a later date. This switch case is extremely redundant and not optimal.
        switch (TutorialManager.Instance.GetCurrentTutorialStage())
        {
            case Tutorial_Stage.Stage_Camera:
                UpdateTutorialElements(TextT1);
                break;
            case Tutorial_Stage.Stage_ClickNest:
                //ToggleT2Buttons();
                UpdateTutorialElements(TextT2);
                break;
            case Tutorial_Stage.Stage_Egg:
                UpdateTutorialElements(TextT3);
                break;
            case Tutorial_Stage.Stage_EggInfo:
                UpdateTutorialElements(TextT4);
                break;
            case Tutorial_Stage.Stage_EggSpeedup:
                UpdateTutorialElements(TextT5);
                break;
            case Tutorial_Stage.Stage_ClickAviary:
                UpdateTutorialElements(TextT6);
                break;
            case Tutorial_Stage.Stage_Store:
                UpdateTutorialElements(TextT7);
                break;
            case Tutorial_Stage.Stage_BuyFood:
                UpdateTutorialElements(TextT8);
                break;
            case Tutorial_Stage.Stage_FeedBird:
                UpdateTutorialElements(TextT9);
                break;
            case Tutorial_Stage.Stage_FedHatchling:
                UpdateTutorialElements(TextT10);
                break;
            case Tutorial_Stage.Stage_HappinessMeter:
                UpdateTutorialElements(TextT11);
                break;
            case Tutorial_Stage.Stage_Hatchling:
                UpdateTutorialElements(TextT12);
                break;
            case Tutorial_Stage.Stage_Tutorial1Finished:
                break;
            case Tutorial_Stage.Stage_Juvenile:
                UpdateTutorialElements(TextT14);
                BirdManager.Instance.GetNestedBird(0).ForceBirdWanderingStatus(true);
                AviaryManager.Instance.UpdateNestStatus();
                break;
            case Tutorial_Stage.Stage_FindJuvenile:
                UpdateTutorialElements(TextT15);
                break;
            case Tutorial_Stage.Stage_ReturnedJuvenile:
                UpdateTutorialElements(TextT16);
                break;
            case Tutorial_Stage.Stage_BackToNest:
                UpdateTutorialElements(TextT17);
                break;
            case Tutorial_Stage.Stage_Adult:
                UpdateTutorialElements(TextT18);
                break;
            case Tutorial_Stage.Stage_AdultBirdMessage:
                UpdateTutorialElements(TextT19);
                break;
            default:
                break;
        }
    }

    public void UpdateTutorialElements(TutorialDisplayText newText)
    {
        if (!string.IsNullOrEmpty(newText.TutorialBodyText))
        {
            BodyText.text = newText.TutorialBodyText;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}

[System.Serializable]
public struct TutorialDisplayText
{
    public int TutorialIndex;
    public string TutorialBodyText;
};
