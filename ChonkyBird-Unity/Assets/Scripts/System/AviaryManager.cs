﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AviaryManager : MonoBehaviour
{
    public static AviaryManager Instance { get; private set; }

    public UI_NotificationPanel notificationPanel;
    public UI_AviaryInteractables nestInteractables;
    private BirdInfo raisedBird;

    private void Awake()
    {
        if (Instance == null)
        {
            //DontDestroyOnLoad(this.gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        /*
        if (TutorialManager.Instance.GetTutorialStatus() && TutorialManager.Instance.GetCurrentTutorialStage() == Tutorial_Stage.Stage_Egg)
        {
            TutorialScreen.Instance.gameObject.SetActive(true);
            //TutorialScreen.Instance.UpdateTutorialIndex();
        }
        */
    }

    public void ToggleNotificationPanel(bool bShow)
    {
        notificationPanel.gameObject.SetActive(bShow);
        
        //! If Tutorial is still active
        if (TutorialManager.Instance.GetTutorialStatus())
        {
            switch (TutorialManager.Instance.GetCurrentTutorialStage())
            {
                case Tutorial_Stage.Stage_Egg:
                    TutorialManager.Instance.IncrementTutorialIndex();
                    break;
                default:
                    break;
            }
        }
    }

    public void DisplayBirdStatus(BirdInfo birdInfo)
    {
        ToggleNotificationPanel(true);
        raisedBird = birdInfo;
        notificationPanel.UpdateBirdInfo(birdInfo);
    }

    public void UpdateBirdName(string newBirdName)
    {
        raisedBird.ChangeBirdName(newBirdName);
    }

    public void UpdateNestStatus()
    {
        nestInteractables.UpdateNestStatus();
    }
}
