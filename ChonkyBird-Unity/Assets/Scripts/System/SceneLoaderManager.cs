﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneLoaderManager : MonoBehaviour
{
    public static SceneLoaderManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void LoadScene(string sceneName)
    {
        //! Let's put all the save functions here whenever loading between menus.
        //! Not sure if this is the best place to put (need to figure out the best system to push for saves, possibly onCloseApp or some similar func)
        //! PlayerAccountManager is ignored for now since it's only their name and that's already saved from the beginning.

        if (BirdManager.Instance != null)
        {
            BirdManager.Instance.SaveBirdsToFile();
        }

        if (InventoryManager.Instance != null)
        {
            InventoryManager.Instance.SaveInventory();
        }

        if (ResourceManager.Instance != null)
        {
            ResourceManager.Instance.SaveResourcesToFile();
        }

        SceneManager.LoadScene(sceneName);
    }
}
