﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager Instance { get; private set; }

    //! Might wanna get rid of SerializeField property later.

    [Header("Current Feeds")]
    [SerializeField] private int BasicFeeds = 0;
    [SerializeField] private int PremiumFeeds = 0;
    [SerializeField] private int LuxuryFeeds = 0;
    [SerializeField] private int ChampionFeeds = 0;

    [Header("Feed Costs")]
    [SerializeField] private static int BasicFeedCost = 50;
    [SerializeField] private static int PremiumFeedCost = 250;
    [SerializeField] private static int LuxuryFeedCost = 500;
    [SerializeField] private static int ChampionFeedCost = 1000;

    [Header("Furniture")]
    [SerializeField] private List<Furniture> OwnedFurniture = new List<Furniture>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }

        LoadInventory();
    }

    //! SaveInventory to futureproof against other inventory items to be saved (e.g. Furniture, etc.)
    public void SaveInventory()
    {
        SaveFeeds();
        //SaveFurniture();
    }

    public void LoadInventory()
    {
        LoadFeeds();
        //LoadFurniture();
    }

    void SaveFeeds()
    {
        SaveManager.SaveInventoryFeeds(BasicFeeds, PremiumFeeds, LuxuryFeeds, ChampionFeeds);
    }

    public void LoadFeeds()
    {
        int[] savedFeeds = SaveManager.LoadInventoryFeeds();
        if (savedFeeds != null)
        {
            BasicFeeds = savedFeeds[0];
            PremiumFeeds = savedFeeds[1];
            LuxuryFeeds = savedFeeds[2];
            ChampionFeeds = savedFeeds[3];
        }
    }

    void SaveFurniture()
    {
        Furniture[] furnitureToSave = OwnedFurniture.ToArray();
        SaveManager.SaveFurnitures(furnitureToSave);
    }

    public void LoadFurniture()
    {
        Furniture[] loadedFurniture = SaveManager.LoadFurnitures();

        if (loadedFurniture != null)
        {
            foreach(Furniture f in loadedFurniture)
            {
                OwnedFurniture.Add(f);
            }
        }
    }

    public void AddFeed(Feed_Type FeedToAdd, int Amount)
    {
        switch (FeedToAdd)
        {
            case Feed_Type.BASIC:
                if (ResourceManager.Instance.GetWP() - (BasicFeedCost * Amount) >= 0)
                {
                    BasicFeeds += Amount;
                    ResourceManager.Instance.DecreaseWP(BasicFeedCost * Amount);
                }
                break;
            case Feed_Type.PREMIUM:
                if (ResourceManager.Instance.GetWP() - PremiumFeedCost * Amount >= 0)
                {
                    PremiumFeeds += Amount;
                    ResourceManager.Instance.DecreaseWP(PremiumFeedCost * Amount);
                }
                break;
            case Feed_Type.LUXURY:
                if (ResourceManager.Instance.GetWP() - LuxuryFeedCost * Amount >= 0)
                {
                    LuxuryFeeds += Amount;
                    ResourceManager.Instance.DecreaseWP(LuxuryFeedCost * Amount);
                }
                break;
            case Feed_Type.CHAMPION:
                if (ResourceManager.Instance.GetWP() - ChampionFeedCost * Amount >= 0)
                {
                    ChampionFeeds += Amount;
                    ResourceManager.Instance.DecreaseWP(ChampionFeedCost * Amount);
                }
                break;
            default:
                break;
        }

        SaveFeeds();
    }

    public void DecreaseFeed(Feed_Type FeedToDecrease)
    {
        switch (FeedToDecrease)
        {
            case Feed_Type.BASIC:
                BasicFeeds--;
                break;
            case Feed_Type.PREMIUM:
                PremiumFeeds--;
                break;
            case Feed_Type.LUXURY:
                LuxuryFeeds--;
                break;
            case Feed_Type.CHAMPION:
                ChampionFeeds--;
                break;
            default:
                break;
        }

        SaveFeeds();
    }

    public int GetFeedAmount()
    {
        return (BasicFeeds + PremiumFeeds + LuxuryFeeds + ChampionFeeds);
    }

    public int GetFeedAmount(Feed_Type feedType)
    {
        switch (feedType)
        {
            case Feed_Type.BASIC:
                return BasicFeeds;
            case Feed_Type.PREMIUM:
                return PremiumFeeds;
            case Feed_Type.LUXURY:
                return LuxuryFeeds;
            case Feed_Type.CHAMPION:
                return ChampionFeeds;
            default:
                return 0;
        }
    }

    public void PurchaseFurniture(Furniture furnitureBought)
    {
        if (ResourceManager.Instance.GetWP() - furnitureBought.wingPointCost >= 0 && ResourceManager.Instance.GetCP() - furnitureBought.chonkPointCost >= 0)
        {
            ResourceManager.Instance.DecreaseWP(furnitureBought.wingPointCost);
            ResourceManager.Instance.DecreaseCP(furnitureBought.chonkPointCost);
            AddFurniture(furnitureBought);
        }
    }

    private void AddFurniture(Furniture newFurniture)
    {
        //! Used to check if there's an available stack of the same furniture that's not at max.
        bool stackAvailable = false;

        //! Cycle through all furnitures
        foreach (Furniture f in OwnedFurniture)
        {
            //! Compare IDs. If true, proceed
            if (f.CompareID(newFurniture.furnitureID))
            {
                //! If it's not at max stack, increase the stack and stop here.
                if (!f.IsAtMaxStack())
                {
                    f.IncreaseStack();
                    stackAvailable = true;
                    return;
                }
                else //! Otherwise mark stackAvailable as false and continue through the loop
                {
                    stackAvailable = false;
                }
            }
        }

        //! If no stack was available the whole time
        if (!stackAvailable)
        {
            //! Create a new stack & copy the info
            Furniture newStack = new Furniture();
            newStack.CopyFurniture(newFurniture);

            OwnedFurniture.Add(newStack);
        }
    }
}
