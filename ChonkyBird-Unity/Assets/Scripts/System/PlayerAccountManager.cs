﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAccountManager : MonoBehaviour
{
    /*
     * Possibly can add functionality here to handle FB/Twitter Connection, need more research
     * Things such as FB ID information and such. Depends on whether there are plugins to handle such things
     * Or they require additional work from our end.
     * Time being, this script will just hold reference to the Player's Name for UI text purposes (if any)
     */

    public static PlayerAccountManager Instance { get; private set; }

    [Header("Player Info")]
    [SerializeField] private string playerName;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    //! This should only ever be called once (when the player is starting out and created their IDs)
    public void SetPlayerName(string name)
    {
        playerName = name;
        SavePlayer();
    }

    public string GetPlayerName()
    {
        return playerName;
    }

    public void SavePlayer()
    {
        SaveManager.SavePlayer(playerName);
    }

    public bool LoadPlayer()
    {
        playerName = SaveManager.LoadPlayer();

        if (playerName != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
