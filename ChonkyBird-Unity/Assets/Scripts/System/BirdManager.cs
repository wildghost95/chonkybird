﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdManager : MonoBehaviour
{
    public static BirdManager Instance { get; private set; }

    #region Private Variables
    [Header("Birds Prefab")]
    [SerializeField] private GameObject BirdPrefab;
    [SerializeField] private List<BirdInfo> NestedBirds = new List<BirdInfo>();
    [SerializeField] private List<BirdInfo> StoredBirds = new List<BirdInfo>();
    [SerializeField] private List<BirdInfo> AdultBirds = new List<BirdInfo>();

    //! Should prob swap.
    Dictionary<string, BirdInfo> NestBirdStats = new Dictionary<string, BirdInfo>();
    Dictionary<string, BirdInfo> StoredBirdStats = new Dictionary<string, BirdInfo>();
    Dictionary<string, BirdInfo> AdultBirdStats = new Dictionary<string, BirdInfo>();

    [Header("Nest")]
    [SerializeField] private int MaxNestSize = 3;
    [SerializeField] private int CurrentNestSize = 0;

    [Header("Egg Storage")]
    [SerializeField] private int MaxEggLimit = 50;
    #endregion

    private void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(this.gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this.gameObject);
        }

        LoadBirdsFromFile();
    }

    private void Start()
    {
        //! Since this is persistent it doesn't get called every time you enter a new scene.
    }

    public void SaveBirdsToFile()
    {
        //! Convert the Birds into an Array since they are by default a List.
        BirdInfo[] nestBirdsToSave = NestedBirds.ToArray();
        SaveManager.SaveNestBirds(nestBirdsToSave);

        BirdInfo[] storedBirdsToSave = StoredBirds.ToArray();
        SaveManager.SaveStoredBirds(storedBirdsToSave);

        BirdInfo[] adultBirdsToSave = AdultBirds.ToArray();
        SaveManager.SaveAdultBirds(adultBirdsToSave);
    }

    public void LoadBirdsFromFile() 
    {
        //! Reverse from Array into a List. Additionally returned info is just saved data.
        //! 1. Get the save data of all birds in Nest
        Bird_SaveData[] loadedNestBirdData = SaveManager.LoadNestBirds();

        if (loadedNestBirdData != null)
        {
            //! Generate a new bird list and modify the data
            foreach (Bird_SaveData data in loadedNestBirdData)
            {
                //! 2. Get a new instanced copy of a bird from the data base.
                BirdInfo tempBird = BirdDatabase.Instance.GetBirdFromDatabase(data.BirdType);
                //! 3. Replace all relevant data with the saved data
                tempBird.LoadSaveData(data);
                //! 4. Add it into the list
                NestedBirds.Add(tempBird);
            }
        }

        //! 5. Repeat for the rest
        Bird_SaveData[] loadedStoredBirdData = SaveManager.LoadStoredBirds();

        if (loadedStoredBirdData != null)
        {
            foreach (Bird_SaveData data in loadedStoredBirdData)
            {
                BirdInfo tempBird = BirdDatabase.Instance.GetBirdFromDatabase(data.BirdType);
                tempBird.LoadSaveData(data);
                StoredBirds.Add(tempBird);
            }
        }

        Bird_SaveData[] loadedAdultBirds = SaveManager.LoadAdultBirds();

        if (loadedAdultBirds != null)
        {
            foreach (Bird_SaveData data in loadedAdultBirds)
            {
                BirdInfo tempBird = BirdDatabase.Instance.GetBirdFromDatabase(data.BirdType);
                tempBird.LoadSaveData(data);
                AdultBirds.Add(tempBird);
            }
        }
    }

    public GameObject GetBirdPrefab()
    {
        return BirdPrefab;
    }

    public void AddNestedBird(string birdKey, BirdInfo birdInfo)
    {
        if (CurrentNestSize + birdInfo.Size <= MaxNestSize)
        {
            NestedBirds.Add(birdInfo);
            CurrentNestSize += birdInfo.Size;
            birdInfo.isIncubating = true;
            birdInfo.UpdateStageDate();

            //! May not need Dictionary to handle Bird Info but rather to store the database of birds instead
            //! Below line commented out just in case need to fall back.
            //NestBirdStats.Add(birdKey, birdInfo);
            SaveBirdsToFile();
        }
    }

    public BirdInfo GetNestedBird(string birdKey)
    {
        //! Used by Dictionary. May become redundant in future
        BirdInfo storedInfo;
        if (NestBirdStats.TryGetValue(birdKey, out storedInfo))
        {
            return storedInfo;
        }
        else
        {
            Debug.Log("Couldn't find associated key!");
            return null;
        }
    }

    public BirdInfo GetNestedBird(int birdIndex)
    {
        if (NestedBirds[birdIndex] != null)
        {
            return NestedBirds[birdIndex];
        }
        else
        {
            return null;
        }
    }

    public List<BirdInfo> GetAllNestedBirds()
    {

        /*
        List<BirdInfo> returnedList = new List<BirdInfo>();
        Dictionary<string, BirdInfo>.ValueCollection valColl = NestBirdStats.Values;

        foreach (BirdInfo bi in valColl)
        {
            returnedList.Add(bi);
        }
        return returnedList;
        */

        return NestedBirds;
    }

    public int GetNestedBirdCount()
    {
        return NestedBirds.Count;
        //return NestBirdStats.Count;
    }

    public void AddAdultBird(BirdInfo bird)
    {
        if (NestedBirds.Contains(bird))
        {
            NestedBirds.Remove(bird);
        }
        AdultBirds.Add(bird);
    }

    public BirdInfo GetAdultBird(int birdIndex)
    {
        if (AdultBirds[birdIndex] != null)
        {
            return AdultBirds[birdIndex];
        }
        else
        {
            return null;
        }
    }
    public List<BirdInfo> GetAllAdultBirds()
    {

        /*
        List<BirdInfo> returnedList = new List<BirdInfo>();
        Dictionary<string, BirdInfo>.ValueCollection valColl = NestBirdStats.Values;

        foreach (BirdInfo bi in valColl)
        {
            returnedList.Add(bi);
        }
        return returnedList;
        */

        return AdultBirds;
    }

    public int GetAdultBirdCount()
    {
        return AdultBirds.Count;
        //return NestBirdStats.Count;
    }

    #region OldStuff
    /*
    public void TranscribeBird(ChonkyBird birdToUpdate)
    {
        if (NestedBirds.Contains(birdToUpdate))
        {
            int index = NestedBirds.IndexOf(birdToUpdate);
            NestedBirds[index].Init(birdToUpdate.Info);

            Debug.Log(NestedBirds[index].Info.STAGE);
        }
        else
        {
            Debug.Log("Transcribing Failed");
        }
    }

    public void AddBirdToStorage(ChonkyBird bird)
    {
        if (StoredBirds.Count + 1 <= MaxEggLimit)
        {
            StoredBirds.Add(bird);
        }
    }

    public void RemoveBirdFromStorage(ChonkyBird bird)
    {
        StoredBirds.Remove(bird);
    }

    public int GetStoredBirdCount()
    {
        return StoredBirds.Count;
    }

    public List<ChonkyBird> GetStoredBirds()
    {
        return StoredBirds;
    }

    public void AddBirdToNest(ChonkyBird bird)
    {
        if (CurrentNestSize + bird.Info.STATS.SIZE <= MaxNestSize)
        {
            CurrentNestSize += bird.Info.STATS.SIZE;
            NestedBirds.Add(bird);
            bird.SetIncubatingStatus(true);
        }
    }

    public void RemoveBirdFromNest(ChonkyBird bird)
    {
        CurrentNestSize -= bird.Info.STATS.SIZE;
        NestedBirds.Remove(bird);
        bird.SetIncubatingStatus(false);
    }

    public int GetNestBirdCount()
    {
        return NestedBirds.Count;
    }

    public List<ChonkyBird> GetNestedBirds()
    {
        return NestedBirds;
    }

    public void AddBirdToAdultList(ChonkyBird bird)
    {
        AdultBirds.Add(bird);
    }

    public void RemoveBirdFromAdultList(ChonkyBird bird)
    {
        AdultBirds.Remove(bird);
    }

    public int GetAdultBirdCount()
    {
        return AdultBirds.Count;
    }

    public List<ChonkyBird> GetAdultBirds()
    {
        return AdultBirds;
    }
    */

    #endregion
}
